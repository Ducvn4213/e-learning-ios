<?php 

class Api { 
    public $data;
    public $conn; 
    
    function insert($table, $data) {
    	$fields = "";
    	$values = "";
    	foreach ($data as $key => $value) {
    		$fields .= "`".$key."`,";
    		$values .= "'".$value."',";
    	}
    	
    	$fields = rtrim($fields, ",");
    	$values = rtrim($values, ",");
    	$sql = "INSERT INTO `".$table."`(".$fields.") VALUES (".$values.")";
    	
    	if ($this->conn->query($sql) === TRUE) {
    		$last_id = $this->conn->insert_id;
    		return $last_id;
		} else {
    		return FALSE;
		}
    }
    
    function update($table, $data, $id) {
    	$sets = "";
    	foreach ($data as $key => $value) {
    		$sets .= "`".$key."`='".$value."',";
    	}
    	$sets = rtrim($sets, ",");
    	$sql = "UPDATE `".$table."` SET ".$sets." WHERE `id`=".$id;
    	
    	if ($this->conn->query($sql) === TRUE) {
   			return TRUE;
		} else {
    		return FALSE;
		}
    }
    
    function select($table, $data) {
    	$wheres = "";
    	foreach ($data as $key => $value) {
    		$wheres .= "`".$key."`='".$value."' AND ";
    	}
    	$wheres = rtrim($wheres, " AND ");
    
    	$sql = "SELECT * FROM `".$table."` WHERE ".$wheres;
    	$result = $this->conn->query($sql);

		if ($result->num_rows > 0) {
    		while($row = $result->fetch_assoc()) {
        		return $row;
    		}
		} else {
    		return FALSE;
		}
    }
	
	function selectAll($table, $data) {
    	$wheres = "";
    	foreach ($data as $key => $value) {
    		$wheres .= "`".$key."`='".$value."' AND ";
    	}
    	$wheres = rtrim($wheres, " AND ");
    
    	$sql = "SELECT * FROM `".$table."` WHERE ".$wheres. "ORDER BY `id` ASC";
    	$result = $this->conn->query($sql);

		$r = array();
		if ($result->num_rows > 0) {
			while($row = $result->fetch_assoc()) {
				$r[] = $row;
    		}
			
			return $r;
		} else {
    		return FALSE;
		}
    }
	
	function selectAllWithoutWhere($table) {
    	$sql = "SELECT * FROM `".$table."";
    	$result = $this->conn->query($sql);

		$r = array();
		if ($result->num_rows > 0) {
			while($row = $result->fetch_assoc()) {
				$r[] = $row;
    		}
			
			return $r;
		} else {
    		return FALSE;
		}
    }
	
	function updateToken($data) {
		$wheres = "";
    	foreach ($data as $key => $value) {
    		$wheres .= "`".$key."`='".$value."' AND ";
    	}
    	$wheres = rtrim($wheres, " AND ");
		
		$chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()_-=+;:,.?";
		$token = substr( str_shuffle( $chars ), 0, 16);
		
		$sql = "UPDATE `user` SET `token`='".$token."' WHERE ".$wheres;
		if ($this->conn->query($sql) === TRUE) {
   			return TRUE;
		} else {
    		return FALSE;
		}
	}
    
    function connectToDB() {
    	include_once("_config.php");
		$this->conn = new mysqli($servername, $username, $password, $database);
		
		if ($this->conn->connect_error) {
			$return_value = array("result"=>false, "data"=>$this->conn->connect_error);
    		echo json_encode($return_value);
    		die();
		} 
    }
    
    function handle() {
    	$this->connectToDB();
    	$this->data = $_POST;
    	
    	$function_name = $this->data["_request"];
    	$this->$function_name();
    }
	
	function getlanguage() {
		$return_value = $this->selectAllWithoutWhere("language");
		//$return_value = array("result"=>true, "data"=>json_encode($return_value, JSON_UNESCAPED_UNICODE));
		echo json_encode($return_value, JSON_UNESCAPED_UNICODE);
	}
	
	function getdata() {
		$data = $this->data["_lang"];
			
		$return_final = array();
		
		$filter = array(
			"language" => $data
		);
		$return_value = $this->selectAll("lesson", $filter);
		foreach ($return_value as $v) {
			$filter = array(
				"lesson_id" => $v['id']
			);
			$return_value = $this->selectAll("lesson_detail", $filter);
			if ($return_value === FALSE) {
				$return_value = array("result"=>false, "data"=>"fail to get detail of lesson");
				echo json_encode($return_value);
				die();
			}
			
			$v['details'] = $return_value;
			$return_final[] = $v;
		}
		echo json_encode($return_final);
	}
}

$api = new Api; 
$api->handle();

?>