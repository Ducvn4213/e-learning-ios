<?php 
header("Content-Type: text/html;charset=UTF-8");

function importFromFile($conn, $name) {
	$json = json_decode($name);
	
	$type = $_POST["type"];
	
	echo $type;
	echo $json;
	
	foreach ($json as $j) {
		if ($type == "1") {
			importFromData_lesson($conn, $j);	
		}
		else if ($type == "2") {
			importFromData_vocabulary($conn, $j);	
		}
		else if ($type == "3") {
			importFromData_phrase($conn, $j);	
		}
		
	}
	
	echo "done";
	header("Location: http://elearning4213.96.lt/index.html");
    exit;
}

function importFromData_phrase($conn, $data) {
	$data = json_encode($data);
	$data = json_decode($data, true);
	$lang = $_POST["code"];
	
	$data_insert = array(
		"type"=>"3",
		"language"=>$lang,
		"audio"=>"",
		"audio2"=>"",
		"title"=>$data['title']
	);
	
	$result = insert($conn, "lesson", $data_insert);
	
	if ($result === FALSE) {
		$return_value = array("result"=>false,"data"=>"fail to create new lesson");
		echo json_encode($return_value);
		die();
	}
	
	$sentences = $data['pharases'];
	foreach ($sentences as $sentence) {
		$data_insert = array(
			"lesson_id"=>$result,
			"title"=>$sentence['text'],
			"meaning"=>$sentence['text_tran'],
			"audio"=>$sentence['normal_audio'],
			"audio_2"=>""
		);
		
		$return_value = insert($conn, "lesson_detail", $data_insert);
		if ($return_value === FALSE) {
			$return_value = array("result"=>false,"data"=>"error during create a lesson detail");
			echo json_encode($return_value);
			die();
		}
	}
}

function importFromData_vocabulary($conn, $data) {
	$data = json_encode($data);
	$data = json_decode($data, true);
	$lang = $_POST["code"];
	
	$data_insert = array(
		"type"=>"2",
		"language"=>$lang,
		"audio"=>"",
		"audio2"=>"",
		"title"=>$data['title']
	);
	
	$result = insert($conn, "lesson", $data_insert);
	
	if ($result === FALSE) {
		$return_value = array("result"=>false,"data"=>"fail to create new lesson");
		echo json_encode($return_value);
		die();
	}
	
	$sentences = $data['vocabularies'];
	foreach ($sentences as $sentence) {
		$data_insert = array(
			"lesson_id"=>$result,
			"title"=>$sentence['text'],
			"meaning"=>$sentence['text_tran'],
			"audio"=>$sentence['audio'],
			"audio_2"=>""
		);
		
		$return_value = insert($conn, "lesson_detail", $data_insert);
		if ($return_value === FALSE) {
			$return_value = array("result"=>false,"data"=>"error during create a lesson detail");
			echo json_encode($return_value);
			die();
		}
	}
}

function importFromData_lesson($conn, $data) {
	
	$data = json_encode($data);
	$data = json_decode($data, true);
	$lang = $_POST["code"];
	
	$data_insert = array(
		"type"=>"1",
		"language"=>$lang,
		"audio"=>$data['normal_audio'],
		"audio2"=>$data['slow_audio'],
		"title"=>$data['title']
	);
	
	$result = insert($conn, "lesson", $data_insert);
	
	if ($result === FALSE) {
		$return_value = array("result"=>false,"data"=>"fail to create new lesson");
		echo json_encode($return_value);
		die();
	}
	
	$sentences = $data['sentences'];
	foreach ($sentences as $sentence) {
		$data_insert = array(
			"lesson_id"=>$result,
			"title"=>$sentence['text'],
			"meaning"=>$sentence['text_tran'],
			"audio"=>$sentence['normal_audio'],
			"audio_2"=>$sentence['slow_audio']
		);
		
		$return_value = insert($conn, "lesson_detail", $data_insert);
		if ($return_value === FALSE) {
			$return_value = array("result"=>false,"data"=>"error during create a lesson detail");
			echo json_encode($return_value);
			die();
		}
	}
}

function insert($conn, $table, $data) {
	$fields = "";
	$values = "";
	foreach ($data as $key => $value) {
		$fields .= "`".$key."`,";
		$values .= "'".mysql_real_escape_string($value)."',";
	}
	
	$fields = rtrim($fields, ",");
	$values = rtrim($values, ",");
	$sql = "INSERT INTO `".$table."`(".$fields.") VALUES (".$values.")";
	
	//echo $sql."</br>"; 
	if ($conn->query($sql) === TRUE) {
		$last_id = $conn->insert_id;
		return $last_id;
	} else {
		return FALSE;
	}
}

function import($conn) {
	$lang = $_POST["code"];
	$langname = $_POST["language"];
	
	$sql = "SELECT * FROM `language` WHERE `value`='".$lang."'";
	$result = $conn->query($sql);
	if ($result->num_rows <= 0) {
		$sql = "INSERT INTO `language`(`value`, `language`) VALUES ('".$lang."','".$langname."')";
		$conn->query($sql);
    }
	
	$filename = $_FILES["type"]["tmp_name"];
	
	$opts = array('http' => array('header' => 'Accept-Charset: UTF-8, *;q=0'));
	$context = stream_context_create($opts);
	
	$content = file_get_contents($filename, false, $context);
	importFromFile($conn, $content);
}


$servername = "localhost";
$username = "u147368984_dg";
$password = "ducgao4213";
$dbname = "u147368984_elern";

$conn = new mysqli($servername, $username, $password, $dbname);
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

import($conn);

?>