<?php 

function importFromFile($conn, $name) {
	
	$filename = $_POST["filename"];
	$name = $filename;
	
	$file = fopen($name, "r") or die("Unable to open file!");
	$content = fread($file,filesize($name));
	$json = json_decode($content);
	fclose($file);
	
	$type = $_POST["type"];
	
	foreach ($json as $j) {
		if ($type == "1") {
			importFromData_lesson($conn, $j);	
		}
		else if ($type == "2") {
			importFromData_vocabulary($conn, $j);	
		}
		else if ($type == "3") {
			importFromData_phrase($conn, $j);	
		}
		
	}
	
	echo "done";
}

function importFromData_phrase($conn, $data) {
	$data = json_encode($data);
	$data = json_decode($data, true);
	$lang = $_POST["lang"];
	
	$data_insert = array(
		"type"=>"3",
		"language"=>$lang,
		"audio"=>"",
		"audio2"=>"",
		"title"=>$data['title']
	);
	
	$result = insert($conn, "lesson", $data_insert);
	
	if ($result === FALSE) {
		$return_value = array("result"=>false,"data"=>"fail to create new lesson");
		echo json_encode($return_value);
		die();
	}
	
	$sentences = $data['pharases'];
	foreach ($sentences as $sentence) {
		$data_insert = array(
			"lesson_id"=>$result,
			"title"=>$sentence['text'],
			"meaning"=>$sentence['text_tran'],
			"audio"=>$sentence['normal_audio'],
			"audio_2"=>""
		);
		
		$return_value = insert($conn, "lesson_detail", $data_insert);
		if ($return_value === FALSE) {
			$return_value = array("result"=>false,"data"=>"error during create a lesson detail");
			echo json_encode($return_value);
			die();
		}
	}
}

function importFromData_vocabulary($conn, $data) {
	$data = json_encode($data);
	$data = json_decode($data, true);
	$lang = $_POST["lang"];
	
	$data_insert = array(
		"type"=>"2",
		"language"=>$lang,
		"audio"=>"",
		"audio2"=>"",
		"title"=>$data['title']
	);
	
	$result = insert($conn, "lesson", $data_insert);
	
	if ($result === FALSE) {
		$return_value = array("result"=>false,"data"=>"fail to create new lesson");
		echo json_encode($return_value);
		die();
	}
	
	$sentences = $data['vocabularies'];
	foreach ($sentences as $sentence) {
		$data_insert = array(
			"lesson_id"=>$result,
			"title"=>$sentence['text'],
			"meaning"=>$sentence['text_tran'],
			"audio"=>$sentence['audio'],
			"audio_2"=>""
		);
		
		$return_value = insert($conn, "lesson_detail", $data_insert);
		if ($return_value === FALSE) {
			$return_value = array("result"=>false,"data"=>"error during create a lesson detail");
			echo json_encode($return_value);
			die();
		}
	}
}

function importFromData_lesson($conn, $data) {
	
	$data = json_encode($data);
	$data = json_decode($data, true);
	$lang = $_POST["lang"];
	
	$data_insert = array(
		"type"=>"1",
		"language"=>$lang,
		"audio"=>$data['normal_audio'],
		"audio2"=>$data['slow_audio'],
		"title"=>$data['title']
	);
	
	$result = insert($conn, "lesson", $data_insert);
	
	if ($result === FALSE) {
		$return_value = array("result"=>false,"data"=>"fail to create new lesson");
		echo json_encode($return_value);
		die();
	}
	
	$sentences = $data['sentences'];
	foreach ($sentences as $sentence) {
		$data_insert = array(
			"lesson_id"=>$result,
			"title"=>$sentence['text'],
			"meaning"=>$sentence['text_tran'],
			"audio"=>$sentence['normal_audio'],
			"audio_2"=>$sentence['slow_audio']
		);
		
		$return_value = insert($conn, "lesson_detail", $data_insert);
		if ($return_value === FALSE) {
			$return_value = array("result"=>false,"data"=>"error during create a lesson detail");
			echo json_encode($return_value);
			die();
		}
	}
}

function insert($conn, $table, $data) {
	$fields = "";
	$values = "";
	foreach ($data as $key => $value) {
		$fields .= "`".$key."`,";
		$values .= '"'.$value.'",';
	}
	
	$fields = rtrim($fields, ",");
	$values = rtrim($values, ",");
	$sql = "INSERT INTO `".$table."`(".$fields.") VALUES (".$values.")";
	
	echo $sql."</br>";
	if ($conn->query($sql) === TRUE) {
		$last_id = $conn->insert_id;
		return $last_id;
	} else {
		return FALSE;
	}
}

$servername = "localhost";
$username = "u147368984_dg";
$password = "ducgao4213";
$dbname = "u147368984_elern";

$conn = new mysqli($servername, $username, $password, $dbname);
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

importFromFile($conn, "lesson_vi.txt");

?>