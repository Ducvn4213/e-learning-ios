//
//  AppDelegate.swift
//  elearning
//
//  Created by DG on 12/7/16.
//  Copyright © 2016 DG. All rights reserved.
//

import UIKit
import UserNotifications
import GoogleMobileAds
import Firebase

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var mService : ELService!

    
    func getELService() -> ELService {
        return mService
    }

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        mService = ELService()
        mService.initService()
        
        if (mService.getTimeNotiEnable() == true) {
            let center = UNUserNotificationCenter.current()
            center.requestAuthorization(options: [.alert, .sound], completionHandler: {(result: Bool, error: Error?) in
                if (result == true) {
                    var hour = 8
                    var minutes = 0
                    let time = self.mService.getSavedTimeNoti()
                    
                    if (time != "") {
                        let timeSplit = time.characters.split(separator: ":").map(String.init)
                        hour = Int(timeSplit[0])!
                        minutes = Int(timeSplit[1])!
                    }
                    
                    var dateComponents = DateComponents()
                    dateComponents.hour = hour
                    dateComponents.minute = minutes
                    let trigger = UNCalendarNotificationTrigger(dateMatching: dateComponents, repeats: true)
                    
                    let content = UNMutableNotificationContent()
                    content.title = "English Learning"
                    content.body = "Let's learn english by conversation"
                    content.sound = UNNotificationSound.default()
                    
                    content.badge = UIApplication.shared.applicationIconBadgeNumber + 1 as NSNumber?
                    let request = UNNotificationRequest(identifier: "every", content: content, trigger: trigger)
                    
                    center.add(request, withCompletionHandler: nil)
                }
            })
        }
        
        FIRApp.configure()
        GADMobileAds.configure(withApplicationID: EL.GOOGLE_ACC_ID)
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

