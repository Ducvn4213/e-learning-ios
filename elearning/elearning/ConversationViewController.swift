import UIKit
import SideMenu
import GoogleMobileAds
import Jukebox

protocol ItemSTTTab {
    func onSTTTab(id : Int, sentence : String)
}

class ConversationViewController: UIViewController, GADBannerViewDelegate, JukeboxDelegate {
    
    @IBOutlet weak var mTableView: UITableView!
    @IBOutlet weak var STTView: UIView!
    @IBOutlet weak var STTText: UILabel!
    
    @IBOutlet weak var mPlayerContainer: UIView!
    @IBOutlet weak var mPlayPauseButton: UIButton!
    @IBOutlet weak var mCurrentTime: UILabel!
    @IBOutlet weak var mEndTime: UILabel!
    @IBOutlet weak var mTimeTrack: UISlider!
    
    
    var currentSentence : String!
    var currentItemId : Int!
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    var mService : ELService!
    
    var mData : [LessionItem] = []
    var mLessonID : Int!
    var mLesson : LessionListItem!
    
    var mJukeBox : Jukebox!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mService = appDelegate.getELService()
        mService.speechToTextCallback = self
        
        STTView.backgroundColor = UIColor.black.withAlphaComponent(0.8)
        STTView.alpha = 0
        STTView.isHidden = true
        
        mTableView.rowHeight = UITableViewAutomaticDimension
        mTableView.estimatedRowHeight = 85
        
        mTimeTrack.value = 0
        
        UIApplication.shared.beginReceivingRemoteControlEvents()
        
        loadData()
        
        initAdmob()
    }
    
    var mShouldChange : Bool = true
    func initAdmob() {
        let screenSize = UIScreen.main.bounds
        let screenWidth = screenSize.width
        
        let tabHeight = self.tabBarController?.tabBar.frame.height
        let navHeight = self.navigationController?.navigationBar.frame.height
        let y = self.view.frame.height - 70 - navHeight! - tabHeight!
        
        let admobView = GADBannerView(frame: CGRect(x: 0, y: y, width: screenWidth, height: 50))
        self.view.addSubview(admobView)
        
        self.mPlayerContainer.frame.size.height = 50
        self.mPlayerContainer.frame.origin.y = admobView.frame.origin.y + 50 + 10
        
        self.mTableView.frame.size.height -= 50
        
        admobView.adUnitID = EL.GOOGLE_ADMOB_ID
        admobView.rootViewController = self
        admobView.delegate = self
        admobView.load(GADRequest())
    }
    
    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
        if (mShouldChange) {
            self.mPlayerContainer.frame.origin.y -= 50
            mShouldChange = false
        }
    }
    
    func loadData() {
        mData = mService.getLessonDetailByLessonID(lesson: mLessonID)
        
        mJukeBox = Jukebox(delegate: self, items: [
            JukeboxItem(URL: NSURL(string: mLesson.audio)! as URL)])
        
        mJukeBox.play()
    }
    
    @IBAction func onDoneSTT(_ sender: Any) {
        onDoneSTT()
    }
    
    @IBAction func onPlayPause(_ sender: Any) {
        if (mJukeBox.state == .paused) {
            mJukeBox.play()
        }
        else {
            mJukeBox.pause()
        }
        
    }
    
    func jukeboxStateDidChange(_ state : Jukebox) {
        if (state.state == .playing) {
            mPlayPauseButton.setImage(UIImage(named: "ic_media_pause"), for: .normal)
        }
        else {
            mPlayPauseButton.setImage(UIImage(named: "ic_media_play"), for: .normal)
        }
    }
    func jukeboxPlaybackProgressDidChange(_ jukebox : Jukebox) {
        if let currentTime = jukebox.currentItem?.currentTime, let duration = jukebox.currentItem?.meta.duration {
            let time = Utils.secondsToHoursMinutesSeconds(seconds: Int(currentTime))
            mCurrentTime.text = String(time.1) + ":" + String(time.2)
            let value = Float(currentTime / duration)
            mTimeTrack.value = value
            
            print(currentTime)
            print(duration)
            
            
            if (duration - currentTime < 0.21) {
                resetUI()
            }
        } else {
            resetUI()
        }
    }
    func jukeboxDidLoadItem(_ jukebox : Jukebox, item : JukeboxItem) {
        if let duration = jukebox.currentItem?.meta.duration {
            let time = Utils.secondsToHoursMinutesSeconds(seconds: Int(duration))
            mEndTime.text = String(time.1) + ":" + String(time.2)
            mJukeBox.pause()
        } else {
            resetUI()
        }
    }
    func jukeboxDidUpdateMetadata(_ jukebox : Jukebox, forItem: JukeboxItem) {
        
    }
    
    func resetUI()
    {
        mCurrentTime.text = "00:00"
        mTimeTrack.value = 0
        mJukeBox.stop()
    }
    
    @IBAction func onSeek(_ sender: Any) {
        if let duration = mJukeBox.currentItem?.meta.duration {
            mJukeBox.seek(toSecond: Int(Double(mTimeTrack.value) * duration))
        }
    }
    
    override func remoteControlReceived(with event: UIEvent?) {}
}

extension ConversationViewController : ItemSTTTab {
    func onSTTTab(id : Int, sentence : String) {
        UIView.animate(withDuration: 0.2, animations: {
            self.STTView.isHidden = false
            self.STTView.alpha = 1
        })
        
        self.currentSentence = sentence
        self.currentItemId = id
        self.STTText.text = "..."
        self.mService.startSTT()
    }
    
    func onDoneSTT() {
        UIView.animate(withDuration: 0.2, animations: {
            self.STTView.alpha = 0
        }, completion: { (Bool) in
            self.STTView.isHidden = true
            self.mService.stopSTT()
            
            for i in 0...(self.mData.count - 1) {
                if (self.mData[i].id == self.currentItemId) {
                    if (self.mData[i].score > 0) {
                        return
                    }
                }
            }
            
            let userSpeakText = self.STTText.text
            let score = Utils.getScore(sentence: self.currentSentence, userSentence: userSpeakText!)
            self.mService.updateScore(id: self.currentItemId, score: score)
            
            if (score > 0) {
                for i in 0...(self.mData.count - 1) {
                    if (self.mData[i].id == self.currentItemId) {
                        self.mData[i].score = score
                        self.mTableView.reloadData()
                        return
                    }
                }
            }
        })
    }
}

extension ConversationViewController : SpeechToTextCallback {
    func onSuccess(text: String) {
        STTText.text = text
    }
    
    func onFail() {
        //TODO
    }
}

extension ConversationViewController : UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return mData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cellIdentifier = "Cell"
        if (indexPath.row % 2 != 0) {
            cellIdentifier = "CellMe"
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! LessionItemCell
        
        let data = mData[indexPath.row]
        
        cell.mData = data
        cell.mService = self.mService
        cell.itemSTTTabDelegate = self
        cell.title.text = data.sentence
        //cell.title.text = "I think I can take the subway to the airport. Do you know where the subway is?"
        cell.title2.text = data.sentence
        cell.meaning.text = data.meaning
        cell.done.rating = Double(data.score)
        cell.done2.rating = Double(data.score)
        
        cell.done.settings.updateOnTouch = false
        cell.done2.settings.updateOnTouch = false
        
        if (indexPath.row % 2 == 0) {
            cell.fixSize()
        }
        else {
            cell.fixSize2()
        }
        
        cell.selectionStyle = .none
        
        mData[indexPath.row].height = Int(cell.container.frame.size.height) + 8
        mData[indexPath.row].coplap_height = Int(cell.container.frame.size.height) + 8
        mData[indexPath.row].margin = Int(mService.originalContainerHeight) - Int(cell.container.frame.size.height)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return CGFloat(mData[indexPath.row].height)
    }
    
    func buttonAction(sender: UIButton!) {
        print("Button tapped")
    }
}


extension ConversationViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        for i in 0 ..< mData.count {
            mData[i].height = mData[i].coplap_height
        }
        
        mData[indexPath.row].height = 400 - mData[indexPath.row].margin
        
        tableView.beginUpdates()
        tableView.endUpdates()
    }
}
