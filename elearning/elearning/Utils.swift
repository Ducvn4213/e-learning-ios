import UIKit
import AVFoundation

class Utils {
    class func getPath(fileName: String) -> String {
        let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
        let fileURL = documentsURL.appendingPathComponent(fileName)
        return fileURL.path
    }
    
    class func copyFile(fileName: NSString) {
        let dbPath: String = getPath(fileName: fileName as String)
        let fileManager = FileManager.default
        if !fileManager.fileExists(atPath: dbPath) {
            let documentsURL = Bundle.main.resourceURL
            let fromPath = documentsURL?.appendingPathComponent(fileName as String)
            do {
                try fileManager.copyItem(atPath: fromPath!.path, toPath: dbPath)
            } catch {}
        }
    }
    
    class func getScore(sentence : String, userSentence : String) -> Int {
        let sentenceWords = sentence.characters.split(separator: " ").map(String.init)
        let userSentence = userSentence.characters.split(separator: " ").map(String.init)
        
        if (userSentence.count < sentenceWords.count) {
            return 0;
        }
        
        for i in 0...(sentenceWords.count - 1) {
            let word = (sentenceWords[i] as String).lowercased()
            let userWord = (userSentence[i] as String).lowercased()
            
            if (!word.contains(userWord)) {
                return 0
            }
        }
        
        return 5
    }
    
    class func secondsToHoursMinutesSeconds (seconds : Int) -> (Int, Int, Int) {
        return (seconds / 3600, (seconds % 3600) / 60, (seconds % 3600) % 60)
    }
}
