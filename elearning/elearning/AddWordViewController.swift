import UIKit
import GoogleMobileAds

class AddWordViewController : UIViewController {
    
    @IBOutlet weak var mYourWord: UITextField!
    @IBOutlet weak var mMeaning: UITextField!
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    var mService : ELService!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mService = appDelegate.getELService()
        
        NotificationCenter.default.addObserver(self, selector: #selector(AddWordViewController.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(AddWordViewController.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIInputViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
    
        initAdmob()
    }
    
    func initAdmob() {
        let screenSize = UIScreen.main.bounds
        let screenWidth = screenSize.width
        
        let tabHeight = self.tabBarController?.tabBar.frame.height
        let y = self.view.frame.height - 50 - tabHeight!
        
        let admobView = GADBannerView(frame: CGRect(x: 0, y: y, width: screenWidth, height: 50))
        self.view.addSubview(admobView)
        
        admobView.adUnitID = EL.GOOGLE_ADMOB_ID
        admobView.rootViewController = self
        admobView.load(GADRequest())
    }
    
    func dismissKeyboard() {
        view.endEditing(true)
    }
    
    func keyboardWillShow(notification: NSNotification) {
        
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0{
                self.view.frame.origin.y -= keyboardSize.height/4
            }
        }
        
    }
    
    func keyboardWillHide(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y != 0{
                self.view.frame.origin.y += keyboardSize.height/4
            }
        }
    }
    
    @IBAction func onCancel(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onAdd(_ sender: Any) {
        let yourWord = mYourWord.text
        let meaning = mMeaning.text
        
        if (yourWord?.isEmpty)! {
            let alertController = UIAlertController(title: "Notice", message: "Your word cannot be empty", preferredStyle: .alert)
            let cancel = UIAlertAction(title: "OK", style: .cancel) { action in }
    
            alertController.addAction(cancel)
            self.present(alertController, animated: true)
            return
        }
        
        if (meaning?.isEmpty)! {
            let alertController = UIAlertController(title: "Notice", message: "Meaning of the word cannot be empty", preferredStyle: .alert)
            let cancel = UIAlertAction(title: "OK", style: .cancel) { action in }
            
            alertController.addAction(cancel)
            self.present(alertController, animated: true)
            return
        }
        
        mService.addWord(word: yourWord!, mean: meaning!)
        self.navigationController?.popViewController(animated: true)
    }
    
}
