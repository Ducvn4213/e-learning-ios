import UIKit

class GameItemCell : UITableViewCell {
    
    var mData : GameItem!
    
    @IBOutlet weak var mText: UILabel!
    @IBOutlet weak var mContainer: UIView!
}
