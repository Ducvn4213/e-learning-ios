import UIKit


class WordSelectGameViewController : UIViewController {
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    var mService : ELService!
    
    var score : Int!
    var left : Int!
    
    @IBOutlet weak var mScore: UILabel!
    @IBOutlet weak var mLeft: UILabel!
    @IBOutlet weak var mLeftIconm: UIImageView!
    
    @IBOutlet weak var mTitle: UILabel!
    @IBOutlet weak var mAudioButton: UIButton!
    @IBOutlet weak var mAnswer1: UIButton!
    @IBOutlet weak var mAnswer2: UIButton!
    @IBOutlet weak var mAnswer3: UIButton!
    @IBOutlet weak var mAnswer4: UIButton!
    
    @IBOutlet weak var mGameDialogTitle: UILabel!
    @IBOutlet weak var mGameScoreResult: UILabel!
    @IBOutlet weak var mGameResultContainer: UIView!
    
    var mCurAns : LessionItem!
    
    var data : [LessionItem] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mService = appDelegate.getELService()
        
        initData()
        
        left = 3
        score = 0
        
        updateScore()
        updateLeft()
        
        mGameResultContainer.alpha = 0
        mGameResultContainer.isHidden = true
    }
    
    func updateScore() {
        mScore.text = String(score)
    }
    
    func updateLeft(){
        
        if (left < 0) {
            showResult()
            return
        }
        
        if (left == 0) {
            mLeftIconm.image = UIImage(named: "quiz_l0")
        }
        else if (left == 1) {
            mLeftIconm.image = UIImage(named: "quiz_l1")
        }
        else if (left == 2) {
            mLeftIconm.image = UIImage(named: "quiz_l2")
        }
        else {
            mLeftIconm.image = UIImage(named: "quiz_l5")
        }
        
        mLeft.text = String(left)
    }
    
    func initData() {
        data.removeAll()
        for gi in mService.gameItems {
            if (gi.meaning.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines) != "") {
                self.data.append(gi)
            }
        }
        
        data.shuffle()
        
        mCurAns = data[0]
        
        mTitle.text = mCurAns.sentence
        
        var block : [LessionItem] = []
        
        block.append(mCurAns)
        block.append((data[1]))
        block.append((data[2]))
        block.append((data[3]))
        
        block.shuffle()
        
        mAnswer1.setTitle(block[0].meaning, for: .normal)
        mAnswer2.setTitle(block[1].meaning, for: .normal)
        mAnswer3.setTitle(block[2].meaning, for: .normal)
        mAnswer4.setTitle(block[3].meaning, for: .normal)
    }
    
    @IBAction func onAudioButtonClick(_ sender: Any) {
        mService.playURL(url: mCurAns.audio)
    }
    
    @IBAction func onAns1Click(_ sender: Any) {
        let mean = mAnswer1.title(for: .normal)
        
        if (mean == mCurAns.meaning) {
            onChooseRight()
        }
        else {
            onChooseWrong()
        }
    }
    
    @IBAction func onAns2Click(_ sender: Any) {
        let mean = mAnswer2.title(for: .normal)
        
        if (mean == mCurAns.meaning) {
            onChooseRight()
        }
        else {
            onChooseWrong()
        }
    }
    
    @IBAction func onAns4Click(_ sender: Any) {
        let mean = mAnswer4.title(for: .normal)
        
        if (mean == mCurAns.meaning) {
            onChooseRight()
        }
        else {
            onChooseWrong()
        }
    }
    
    @IBAction func onAns3Click(_ sender: Any) {
        let mean = mAnswer3.title(for: .normal)
        
        if (mean == mCurAns.meaning) {
            onChooseRight()
        }
        else {
            onChooseWrong()
        }
    }
    
    func onChooseRight() {
        score = score + 10
        updateScore()
        showNext(right: true);
    }
    
    func onChooseWrong() {
        left = left - 1
        updateLeft()
        showNext(right: false)
    }
    
    func showResult() {
        mGameScoreResult.text = String(score)
        if (left < 0) {
            mGameDialogTitle.text = "GAME OVER"
        }
        else {
            mGameDialogTitle.text = "YOU WIN"
        }
        
        UIView.animate(withDuration: 0.2, animations: {
            self.mGameResultContainer.isHidden = false
            self.mGameResultContainer.alpha = 1
        });
    }
    
    func showNext(right : Bool) {
        var title = "Wrong"
        if (right == true) {
            title = "Correct"
        }
        
        let mess = mCurAns.sentence + "\n" + mCurAns.meaning
        
        let alertController1 = UIAlertController(title: title, message: mess, preferredStyle: .alert)
        let next = UIAlertAction(title: "Next Quiz", style: .default) { action in
            self.initData()
        }
        
        alertController1.addAction(next)
        
        self.present(alertController1, animated: true)
    }
    
    @IBAction func onTryAgain(_ sender: Any) {
        initData()
        left = 3
        score = 0
        updateScore()
        updateLeft()
        UIView.animate(withDuration: 0.2, animations: {
            self.mGameResultContainer.alpha = 0
        }, completion: { (valuee : Bool) in
            self.mGameResultContainer.isHidden = true
        })
    }
    
    @IBAction func onExit(_ sender: Any) {
        UIView.animate(withDuration: 0.2, animations: {
            self.mGameResultContainer.alpha = 0
        }, completion: { (valuee : Bool) in
            self.mGameResultContainer.isHidden = true
            
            self.navigationController?.popViewController(animated: true)
        })
    }
}
