import UIKit
import GoogleMobileAds
import KTCenterFlowLayout

class WordCompleteGameViewController : UIViewController {
    @IBOutlet weak var mCollectionView: UICollectionView!
    @IBOutlet weak var mCollectionView2: UICollectionView!
    
    @IBOutlet weak var mScore: UILabel!
    @IBOutlet weak var mLeft: UILabel!
    @IBOutlet weak var mLeftIconm: UIImageView!
    @IBOutlet weak var mTitle: UILabel!
    
    @IBOutlet weak var mGameDialogTitle: UILabel!
    @IBOutlet weak var mGameScoreResult: UILabel!
    @IBOutlet weak var mGameResultContainer: UIView!
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    var mService : ELService!
    
    var data : [LessionItem] = []
    var curLessionItem : LessionItem!
    var resultExpert : [String] = []
    var dataShow : [WCGameItem] = []
    var dataChoosen : [WCGameItem] = []
    var nextCharacterNeed : String!
    var indexNeed : Int!
    
    var score : Int!
    var left : Int!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mService = appDelegate.getELService()
        
        loadGameData()
        
        initAdmob()
        
        left = 3
        score = 0
        
        updateScore()
        updateLeft()
        
        mGameResultContainer.alpha = 0
        mGameResultContainer.isHidden = true
        
        let layout = KTCenterFlowLayout()
        layout.minimumInteritemSpacing = 10.0
        layout.minimumLineSpacing = 10.0
        
        mCollectionView2.collectionViewLayout = layout
    }
    
    func updateScore() {
        mScore.text = String(score)
    }
    
    func updateLeft(){
        
        if (left < 0) {
            showResult()
            return
        }
        
        if (left == 0) {
            mLeftIconm.image = UIImage(named: "quiz_l0")
        }
        else if (left == 1) {
            mLeftIconm.image = UIImage(named: "quiz_l1")
        }
        else if (left == 2) {
            mLeftIconm.image = UIImage(named: "quiz_l2")
        }
        else {
            mLeftIconm.image = UIImage(named: "quiz_l5")
        }
        
        mLeft.text = String(left)
    }
    
    func showResult() {
        mGameScoreResult.text = String(score)
        if (left < 0) {
            mGameDialogTitle.text = "GAME OVER"
        }
        else {
            mGameDialogTitle.text = "YOU WIN"
        }
        
        UIView.animate(withDuration: 0.2, animations: {
            self.mGameResultContainer.isHidden = false
            self.mGameResultContainer.alpha = 1
        });
    }
    
    func onChooseRight() {
        score = score + 10
        updateScore()
    }
    
    func onChooseWrong() {
        left = left - 1
        updateLeft()
        //showNext(right: false)
    }
    
    func showNext() {
        let title = "Correct"
        
        let mess = curLessionItem.sentence + "\n" + curLessionItem.meaning
        
        let alertController1 = UIAlertController(title: title, message: mess, preferredStyle: .alert)
        let next = UIAlertAction(title: "Next Quiz", style: .default) { action in
            self.loadGameData()
        }
        
        alertController1.addAction(next)
        
        self.present(alertController1, animated: true)
    }
    
    func loadGameData() {
        indexNeed = 0
        data.removeAll()
        dataShow.removeAll()
        for gi in mService.gameItems {
            if (gi.meaning.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines) != "") {
                self.data.append(gi)
            }
        }
        
        data.shuffle()
        
        curLessionItem = data[0]
        
        mTitle.text = curLessionItem.meaning
        
        resultExpert.removeAll()
        
        
        for i in 0..<curLessionItem.sentence.characters.count {
            let index = curLessionItem.sentence.index(curLessionItem.sentence.startIndex, offsetBy: i)
            resultExpert.append(String(curLessionItem.sentence[index]))
        }
        
        for it in resultExpert {
            let wcgi = WCGameItem(letter: it, isDone: false)
            dataShow.append(wcgi)
        }
        
        dataShow.shuffle()
        
        nextCharacterNeed = resultExpert[0]
        
        dataChoosen.removeAll()
        
        mCollectionView.reloadData()
        mCollectionView2.reloadData()
    }
    
    func initAdmob() {
        let screenSize = UIScreen.main.bounds
        let screenWidth = screenSize.width
        
        let tabHeight = self.tabBarController?.tabBar.frame.height
        let y = self.view.frame.height - 50 - tabHeight!
        
        let admobView = GADBannerView(frame: CGRect(x: 0, y: y, width: screenWidth, height: 50))
        self.view.addSubview(admobView)
        
        admobView.adUnitID = EL.GOOGLE_ADMOB_ID
        admobView.rootViewController = self
        admobView.load(GADRequest())
    }
    
    @IBAction func onExit(_ sender: Any) {
        UIView.animate(withDuration: 0.2, animations: {
            self.mGameResultContainer.alpha = 0
        }, completion: { (valuee : Bool) in
            self.mGameResultContainer.isHidden = true
            
            self.navigationController?.popViewController(animated: true)
        })
    }
    
    @IBAction func onTryAgain(_ sender: Any) {
        loadGameData()
        left = 3
        score = 0
        updateScore()
        updateLeft()
        UIView.animate(withDuration: 0.2, animations: {
            self.mGameResultContainer.alpha = 0
        }, completion: { (valuee : Bool) in
            self.mGameResultContainer.isHidden = true
        })
    }
}

extension WordCompleteGameViewController : UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if (collectionView == mCollectionView2) {
            return dataChoosen.count
        }
        return dataShow.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if (collectionView == mCollectionView) {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath as IndexPath) as! WCItemCell
            
            let gameItem = dataShow[indexPath.row]
            
            cell.mText.text = gameItem.letter
            
            if (gameItem.isDone) {
                cell.isHidden = true
                cell.mContainer.isHidden = true
                cell.mText.isHidden = true
            }
            else {
                cell.isHidden = false
                cell.mContainer.isHidden = false
                cell.mText.isHidden = false
            }
            
            return cell
        }
        else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell2", for: indexPath as IndexPath) as! WCItemCell
            
            let gameItem = dataChoosen[indexPath.row]
            
            cell.mText.text = gameItem.letter
            
            if (gameItem.isDone) {
                cell.isHidden = true
                cell.mContainer.isHidden = true
                cell.mText.isHidden = true
            }
            else {
                cell.isHidden = false
                cell.mContainer.isHidden = false
                cell.mText.isHidden = false
            }
            
            return cell
        }
    }
}

extension WordCompleteGameViewController : UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        var numberOfCell: CGFloat = 5
        
        if (collectionView == mCollectionView2) {
            numberOfCell = 10
        }
        
        let cellWidth = (UIScreen.main.bounds.size.width / numberOfCell) - 10
        return CGSize(width: cellWidth, height: cellWidth)
    }
}

extension WordCompleteGameViewController : UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        //let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath as IndexPath) as! WCItemCell
        
        if (collectionView == mCollectionView2) {
            return
        }
        
        let gameItem = dataShow[indexPath.row]
        
        let pick = gameItem.letter
        
        
        if (pick == nextCharacterNeed) {
            dataShow[indexPath.row].isDone = true
            indexNeed = indexNeed + 1
            if (indexNeed == resultExpert.count) {
                showNext()
            }
            else {
                nextCharacterNeed = resultExpert[indexNeed]
            }
            onChooseRight()
            dataChoosen.append(gameItem)
        }
        else {
            onChooseWrong()
        }
        
        mCollectionView.reloadData()
        mCollectionView2.reloadData()
    }
}
