import UIKit
import SideMenu
import GoogleMobileAds
import FBAudienceNetwork


class PhraseListViewController : UIViewController, FBAdViewDelegate {
    var mData : [LessionListItem] = []
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    var mService : ELService!
    
    @IBOutlet weak var mCollectionView: UICollectionView!
    @IBOutlet weak var mLoadingView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mService = appDelegate.getELService()
        
        setupMenu()
        
        initFaceAdmob()
    }
    
    func initAdmob() {
        let screenSize = UIScreen.main.bounds
        let screenWidth = screenSize.width
        
        let tabHeight = self.tabBarController?.tabBar.frame.height
        let y = self.view.frame.height - 50 - tabHeight!
        
        let admobView = GADBannerView(frame: CGRect(x: 0, y: y, width: screenWidth, height: 50))
        self.view.addSubview(admobView)
        
        admobView.adUnitID = EL.GOOGLE_ADMOB_ID
        admobView.rootViewController = self
        admobView.load(GADRequest())
    }
    
    func initFaceAdmob() {
        FBAdSettings.addTestDevice(FBAdSettings.testDeviceHash())
        let adView = FBAdView(placementID: EL.FACEBOOK_ADMOB_ID, adSize: kFBAdSize320x50, rootViewController: self)
        
        adView.delegate = self
        
        let screenSize = UIScreen.main.bounds
        let screenWidth = screenSize.width
        
        let tabHeight = self.tabBarController?.tabBar.frame.height
        let y = self.view.frame.height - 50 - tabHeight!
        adView.frame = CGRect(x: 0, y: y, width: screenWidth, height: 50)
        self.view.addSubview(adView)
        
        adView.loadAd()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.mData = self.mService.getPhraseLesson()
        calculateToUnlockLesson()
        mCollectionView.reloadData()
        mLoadingView.isHidden = true
        
        self.mService.viewControllerBeforeGoToLanguage = self
    }
    
    func calculateToUnlockLesson() {
        var item_index = 1
        var pass_num = 0
        for (index, data) in self.mData.enumerated() {
            if (data.score >= data.needed_score) {
                pass_num += 1
            }
            
            if (item_index == 5) {
                if (pass_num == 5) {
                    unlockNext5(from: index)
                    return
                }
                item_index = 0
                pass_num = 0
            }
            
            item_index += 1
        }
    }
    
    func unlockNext5(from : Int) {
        for i in 1...5 {
            let index = from + i
            if (index < self.mData.count) {
                self.mData[index].lock = 0
            }
        }
    }
    
    func setupMenu() {
        SideMenuManager.menuLeftNavigationController = storyboard!.instantiateViewController(withIdentifier: "LeftMenuNavigationController") as? UISideMenuNavigationController
        
        SideMenuManager.menuAddPanGestureToPresent(toView: self.navigationController!.navigationBar)
        SideMenuManager.menuAddScreenEdgePanGesturesToPresent(toView: self.navigationController!.view)
    }
}


extension PhraseListViewController : UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return mData.count;
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        var cell : LessionListItemCell!
        
        let data = mData[indexPath.row]
        if (data.lock == 1) {
            cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell_Lock", for: indexPath as IndexPath) as! LessionListItemCell
            cell.title.text = data.title
            cell.lock_container.layer.cornerRadius = 15
            cell.lock_container.layer.borderWidth = 1
            cell.lock_container.layer.borderColor = UIColor.white.cgColor
            cell.lock_container.layer.masksToBounds = true
        }
        else {
            cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath as IndexPath) as! LessionListItemCell
            cell.title.text = data.title
            cell.score.text = "Your point: " + String(data.score) + "/" + String(data.max_score)
            cell.needed_score.text = "You need: " + String(data.needed_score) + " point to pass this lesson"

            if (data.score >= data.needed_score) {
                cell.bg_need_pass.image = UIImage(named: "bg_pass")
                cell.bg_need_pass.isHidden = false
            }
            else {
                cell.bg_need_pass.isHidden = true
                            }
        }
        
        return cell
    }
}

extension PhraseListViewController : UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let numberOfCell: CGFloat = 1
        let cellWidth = (UIScreen.main.bounds.size.width / numberOfCell) - 10
        return CGSize(width: cellWidth, height: 80)
    }
}

extension PhraseListViewController : UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let data = mData[indexPath.row]
        if (data.lock == 1) {
            let alertController = UIAlertController(title: "Locked", message: "Try to pass previous lesson to unlock this lesson", preferredStyle: .alert)
            
            let action = UIAlertAction(title: "OK", style: .default) { action in }
            alertController.addAction(action)
            
            self.present(alertController, animated: true)
            return
        }
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let detailViewController = storyBoard.instantiateViewController(withIdentifier: "phrase") as! PhraseViewController
        detailViewController.mLessonID = data.id
        self.navigationController?.pushViewController(detailViewController, animated: true)
    }
}
