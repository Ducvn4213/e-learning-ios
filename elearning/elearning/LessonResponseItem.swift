import JASON

extension JSONKeys {
    static let type    = JSONKey<String>("type")
    static let audio2    = JSONKey<String>("audio2")
    static let image    = JSONKey<String>("image")
    static let lessonDetail = JSONKey<JSON>("details")
}

struct LessonResponseItem {
    var id : Int
    var type : Int
    var language : String
    var audio : String
    var audio2 : String
    var image : String
    var score : Int
    var title : String
    var detail : [LessonDetail]
    
    init(_ json: JSON) {
        id    = Int(json[.iid])!
        type = Int(json[.type])!
        language = json[.language]
        audio = json[.audio]
        audio2 = json[.audio2]
        image = json[.language]
        score = 0
        title = json[.title]
        detail = json[.lessonDetail].map(LessonDetail.init)
    }
}
