import UIKit

class LessionListItemCell: UICollectionViewCell {
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var needed_score: UILabel!
    @IBOutlet weak var score: UILabel!
    @IBOutlet weak var lock_container: UIView!
    @IBOutlet weak var bg_need_pass: UIImageView!
}
