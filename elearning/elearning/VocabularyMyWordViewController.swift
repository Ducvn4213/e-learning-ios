import UIKit
import SideMenu
import GoogleMobileAds

class VocabularyMyWordViewController : UIViewController {
    
    @IBOutlet weak var mTableView: UITableView!
    @IBOutlet weak var STTView: UIView!
    @IBOutlet weak var STTText: UILabel!
    
    var currentSentence : String!
    var currentTimeSave : Int!
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    var mService : ELService!
    
    var mData : [LessionItem] = []
    var mLessonID : Int!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mService = appDelegate.getELService()
        mService.speechToTextCallback = self
        
        STTView.backgroundColor = UIColor.black.withAlphaComponent(0.8)
        STTView.alpha = 0
        STTView.isHidden = true
        
        mTableView.rowHeight = UITableViewAutomaticDimension
        mTableView.estimatedRowHeight = 85
        
        loadData()
    
        initAdmob()
    }
    
    func initAdmob() {
        let screenSize = UIScreen.main.bounds
        let screenWidth = screenSize.width
        
        let tabHeight = self.tabBarController?.tabBar.frame.height
        let y = self.view.frame.height - 50 - tabHeight!
        
        let admobView = GADBannerView(frame: CGRect(x: 0, y: y, width: screenWidth, height: 50))
        self.view.addSubview(admobView)
        
        admobView.adUnitID = EL.GOOGLE_ADMOB_ID
        admobView.rootViewController = self
        admobView.load(GADRequest())
    }
    
    override func viewDidAppear(_ animated: Bool) {
        loadData()
    }
    
    func loadData() {
        mData = mService.getMyWord()
        for i in 0 ..< mData.count {
            mData[i].height = 92
        }
        mTableView.reloadData()
    }
    
    @IBAction func onDoneSTT(_ sender: Any) {
        onDoneSTT()
    }
    
    @IBAction func onAction(_ sender: Any) {
        let alertController = UIAlertController(title: "", message: "SELECT WHAT YOU WANT", preferredStyle: .actionSheet)
        
        let game = UIAlertAction(title: "ADD A WORD", style: .default) { action in
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let viewController = storyBoard.instantiateViewController(withIdentifier: "addword")
            self.navigationController?.pushViewController(viewController, animated: true)
        }
        let add = UIAlertAction(title: "PLAY GAME", style: .default) { action in
            let alertController1 = UIAlertController(title: "", message: "SELECT WHAT YOU WANT", preferredStyle: .alert)
            let ws = UIAlertAction(title: "Word Select", style: .default) { action in
                self.openGame1()
            }
            let wc = UIAlertAction(title: "Word Complete", style: .default) { action in
                self.openGame2()
            }
            let wfc = UIAlertAction(title: "Word Flash Card", style: .default) { action in
                self.openGame3()
            }
            let mw = UIAlertAction(title: "Match Word", style: .default) { action in
                self.openGame4()
            }
            
            alertController1.addAction(ws)
            alertController1.addAction(wc)
            alertController1.addAction(wfc)
            alertController1.addAction(mw)
            
            self.present(alertController1, animated: true)
        }
        let cancel = UIAlertAction(title: "Cancel", style: .cancel) { action in }

        alertController.addAction(game)
        alertController.addAction(add)
        alertController.addAction(cancel)
        
        self.present(alertController, animated: true)
    }
    
    func openGame1() {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        self.mService.gameItems = self.mData
        
        if (self.mData.count < 6) {
            let ac = UIAlertController(title: "NOTICE", message: "You must have at least 6 words to play the game", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style: .default) { action in }
            
            ac.addAction(ok)
            self.present(ac, animated: true)
        }
        else {
            let viewController = storyBoard.instantiateViewController(withIdentifier: "game1")
            self.navigationController?.pushViewController(viewController, animated: true)
        }
    }
    
    func openGame2() {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        self.mService.gameItems = self.mData
        
        if (self.mData.count < 6) {
            let ac = UIAlertController(title: "NOTICE", message: "You must have at least 6 words to play the game", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style: .default) { action in }
            
            ac.addAction(ok)
            self.present(ac, animated: true)
        }
        else {
            let viewController = storyBoard.instantiateViewController(withIdentifier: "game2")
            self.navigationController?.pushViewController(viewController, animated: true)
        }
    }
    
    func openGame3() {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        self.mService.gameItems = self.mData
        
        if (self.mData.count < 6) {
            let ac = UIAlertController(title: "NOTICE", message: "You must have at least 6 words to play the game", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style: .default) { action in }
            
            ac.addAction(ok)
            self.present(ac, animated: true)
        }
        else {
            let viewController = storyBoard.instantiateViewController(withIdentifier: "game3")
            self.navigationController?.pushViewController(viewController, animated: true)
        }
    }
    
    func openGame4() {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        self.mService.gameItems = self.mData
        
        if (self.mData.count < 6) {
            let ac = UIAlertController(title: "NOTICE", message: "You must have at least 6 words to play the game", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style: .default) { action in }
            
            ac.addAction(ok)
            self.present(ac, animated: true)
        }
        else {
            let viewController = storyBoard.instantiateViewController(withIdentifier: "game")
            self.navigationController?.pushViewController(viewController, animated: true)
        }
    }
    
}

extension VocabularyMyWordViewController : updateLocalDataDelegate {
    func updateLocalData(id: Int, timesave: Int) {
        for i in 0 ..< mData.count {
            if (mData[i].id == id) {
                mData[i].timesave = timesave
                if (timesave == 0) {
                    mData.remove(at: i)
                    mTableView.reloadData()
                }
                return
            }
        }
    }
}

extension VocabularyMyWordViewController : ItemSTTTab {
    func onSTTTab(id : Int, sentence : String) {
        UIView.animate(withDuration: 0.2, animations: {
            self.STTView.isHidden = false
            self.STTView.alpha = 1
        })
        
        self.currentSentence = sentence
        self.currentTimeSave = id
        self.STTText.text = "..."
        self.mService.startSTT()
    }
    
    func onDoneSTT() {
        UIView.animate(withDuration: 0.2, animations: {
            self.STTView.alpha = 0
        }, completion: { (Bool) in
            self.STTView.isHidden = true
            self.mService.stopSTT()
        })
        
        for i in 0...(self.mData.count - 1) {
            if (self.mData[i].timesave == self.currentTimeSave) {
                if (self.mData[i].score > 0) {
                    return
                }
            }
        }
        
        let userSpeakText = self.STTText.text
        let score = Utils.getScore(sentence: self.currentSentence, userSentence: userSpeakText!)
        self.mService.updateScoreByTimeSave(timesave: self.currentTimeSave, score: score)
        
        if (score > 0) {
            for i in 0...(self.mData.count - 1) {
                if (self.mData[i].timesave == self.currentTimeSave) {
                    self.mData[i].score = score
                    self.mTableView.reloadData()
                    return
                }
            }
        }
    }
}

extension VocabularyMyWordViewController : SpeechToTextCallback {
    func onSuccess(text: String) {
        STTText.text = text
    }
    
    func onFail() {
        //TODO
    }
}

extension VocabularyMyWordViewController : UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return mData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = "Cell"
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! VocabularyItemCell
        
        let data = mData[indexPath.row]
        
        cell.mData = data
        cell.mService = self.mService
        cell.itemSTTTabDelegate = self
        cell.updateLocalDataDelegate = self
        
        cell.title.text = data.sentence
        cell.title2.text = data.sentence
        
        cell.meaning.text = data.meaning
        cell.score.rating = Double(data.score)
        cell.score2.rating = Double(data.score)
        cell.score.settings.updateOnTouch = false
        cell.score2.settings.updateOnTouch = false
        
        if (data.audio.isEmpty) {
            cell.speaker.isHidden = true
        }
        else {
            cell.speaker.isHidden = false
        }
        
        if (data.timesave > 0) {
            cell.bookmar.setImage(UIImage(named: "bookmar_full"), for: .normal)
        }
        else {
            cell.bookmar.setImage(UIImage(named: "bookmar_empty"), for: .normal)
        }
        
        cell.selectionStyle = .none
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return CGFloat(mData[indexPath.row].height)
    }
    
    func buttonAction(sender: UIButton!) {
        print("Button tapped")
    }
}


extension VocabularyMyWordViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        for i in 0 ..< mData.count {
            mData[i].height = 92
        }
        
        mData[indexPath.row].height = 320
        
        tableView.beginUpdates()
        tableView.endUpdates()
    }
}
