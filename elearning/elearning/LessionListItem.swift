struct LessionListItem {
    var id : Int
    var score: Int
    var max_score : Int
    var needed_score : Int
    var image : String
    var title : String
    var lock : Int
    var audio : String
}
