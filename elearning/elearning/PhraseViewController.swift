import UIKit
import SideMenu
import GoogleMobileAds

class PhraseViewController : UIViewController {
    
    @IBOutlet weak var mTableView: UITableView!
    @IBOutlet weak var STTView: UIView!
    @IBOutlet weak var STTText: UILabel!
    
    var currentSentence : String!
    var currentItemId : Int!
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    var mService : ELService!
    
    var mData : [LessionItem] = []
    var mLessonID : Int!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mService = appDelegate.getELService()
        mService.speechToTextCallback = self
        
        STTView.backgroundColor = UIColor.black.withAlphaComponent(0.8)
        STTView.alpha = 0
        STTView.isHidden = true
        
        mTableView.rowHeight = UITableViewAutomaticDimension
        mTableView.estimatedRowHeight = 85
        
        loadData()
    
        initAdmob()
    }
    
    func initAdmob() {
        let screenSize = UIScreen.main.bounds
        let screenWidth = screenSize.width
        
        let tabHeight = self.tabBarController?.tabBar.frame.height
        let y = self.view.frame.height - 50 - tabHeight!
        
        let admobView = GADBannerView(frame: CGRect(x: 0, y: y, width: screenWidth, height: 50))
        self.view.addSubview(admobView)
        
        admobView.adUnitID = EL.GOOGLE_ADMOB_ID
        admobView.rootViewController = self
        admobView.load(GADRequest())
    }
    
    func loadData() {
        mData = mService.getLessonDetailByLessonID(lesson: mLessonID)
    }
    
    @IBAction func onDoneSTT(_ sender: Any) {
        onDoneSTT()
    }
}

extension PhraseViewController : ItemSTTTab {
    func onSTTTab(id : Int, sentence : String) {
        UIView.animate(withDuration: 0.2, animations: {
            self.STTView.isHidden = false
            self.STTView.alpha = 1
        })
        
        self.currentSentence = sentence
        self.currentItemId = id
        self.STTText.text = "..."
        self.mService.startSTT()
    }
    
    func onDoneSTT() {
        UIView.animate(withDuration: 0.2, animations: {
            self.STTView.alpha = 0
        }, completion: { (Bool) in
            self.STTView.isHidden = true
            self.mService.stopSTT()
        })
        
        for i in 0...(self.mData.count - 1) {
            if (self.mData[i].id == self.currentItemId) {
                if (self.mData[i].score > 0) {
                    return
                }
            }
        }
        
        let userSpeakText = self.STTText.text
        let score = Utils.getScore(sentence: self.currentSentence, userSentence: userSpeakText!)
        self.mService.updateScore(id: self.currentItemId, score: score)
        
        if (score > 0) {
            for i in 0...(self.mData.count - 1) {
                if (self.mData[i].id == self.currentItemId) {
                    self.mData[i].score = score
                    self.mTableView.reloadData()
                    return
                }
            }
        }
    }
}

extension PhraseViewController : SpeechToTextCallback {
    func onSuccess(text: String) {
        STTText.text = text
    }
    
    func onFail() {
        //TODO
    }
}

extension PhraseViewController : UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return mData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = "Cell"
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! LessionItemCell
        
        let data = mData[indexPath.row]
        
        cell.mData = data
        cell.mService = self.mService
        cell.itemSTTTabDelegate = self
        cell.title.text = data.sentence
        //cell.title.text = "I think I can take the subway to the airport. Do you know where the subway is?"
        cell.title2.text = data.sentence
        cell.meaning.text = data.meaning
        cell.done.rating = Double(data.score)
        cell.done2.rating = Double(data.score)
        
        cell.done.settings.updateOnTouch = false
        cell.done2.settings.updateOnTouch = false
        
        cell.fixSize()
        
        cell.selectionStyle = .none
        
        mData[indexPath.row].height = Int(cell.container.frame.size.height) + 8
        mData[indexPath.row].coplap_height = Int(cell.container.frame.size.height) + 8
        mData[indexPath.row].margin = Int(mService.originalContainerHeight) - Int(cell.container.frame.size.height)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return CGFloat(mData[indexPath.row].height)
    }
    
    func buttonAction(sender: UIButton!) {
        print("Button tapped")
    }
}


extension PhraseViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        for i in 0 ..< mData.count {
            mData[i].height = mData[i].coplap_height
        }
        
        mData[indexPath.row].height = 400 - mData[indexPath.row].margin
        
        tableView.beginUpdates()
        tableView.endUpdates()
    }
}
