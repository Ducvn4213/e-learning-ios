import UIKit
import Cosmos

class VocabularyItemCell: UITableViewCell {
    var mData : LessionItem!
    var mService : ELService!
    var itemSTTTabDelegate : ItemSTTTab!
    var updateLocalDataDelegate: updateLocalDataDelegate!
  
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var title2: UILabel!
    @IBOutlet weak var meaning: UILabel!
    @IBOutlet weak var score: CosmosView!
    @IBOutlet weak var score2: CosmosView!
    @IBOutlet weak var bookmar: UIButton!
    @IBOutlet weak var speaker: UIButton!
    
    @IBAction func onSpeak(_ sender: Any) {
        mService.playURL(url: mData.audio)
    }
    
    @IBAction func onUserSpeak(_ sender: Any) {
        if (self.mData.id == 0) {
            itemSTTTabDelegate.onSTTTab(id: self.mData.timesave, sentence: self.mData.sentence)
        }
        else {
            itemSTTTabDelegate.onSTTTab(id: self.mData.id, sentence: self.mData.sentence)
        }
    }
    
    @IBAction func onSetMyWord(_ sender: Any) {
        if (mData.timesave > 0) {
            mService.removeFromMyWord(id: mData.id)
            bookmar.setImage(UIImage(named: "bookmar_empty"), for: .normal)
            updateLocalDataDelegate.updateLocalData(id: mData.id, timesave: 0)
            mData.timesave = 0
        }
        else {
            mService.addToMyWord(id: mData.id)
            bookmar.setImage(UIImage(named: "bookmar_full"), for: .normal)
            updateLocalDataDelegate.updateLocalData(id: mData.id, timesave: 1)
            mData.timesave = 1
        }
    }
}
