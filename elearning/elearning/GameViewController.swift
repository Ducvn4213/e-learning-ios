import UIKit


class GameViewController : UIViewController {
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    var mService : ELService!
    
    @IBOutlet weak var mTableWord: UITableView!
    @IBOutlet weak var mTableMean: UITableView!
    
    @IBOutlet weak var mScore: UILabel!
    @IBOutlet weak var mLeft: UILabel!
    @IBOutlet weak var mLeftIconm: UIImageView!
    
    @IBOutlet weak var mGameDialogTitle: UILabel!
    @IBOutlet weak var mGameScoreResult: UILabel!
    @IBOutlet weak var mGameResultContainer: UIView!
    
    var mWordList : [GameItem] = []
    var mMeanList : [GameItem] = []
    
    var mSelectedGameItem : GameItem!
    
    var score : Int!
    var left : Int!
    
    var isSelectedWord : Int = -1
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mService = appDelegate.getELService()
        
        initData()
        
        mGameResultContainer.alpha = 0
        mGameResultContainer.isHidden = true
    }
    
    func initData() {
        mWordList.removeAll()
        mMeanList.removeAll()
        
        var data = mService.gameItems
        data?.shuffle()
        
        var cur = 0
        for ls in data! {
            if (cur > 10) {
                break
            }
            
            if (ls.meaning.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines) != "") {
                cur += 1
                let gi = GameItem(word: ls.sentence, mean : ls.meaning, isSelected: false)
                mWordList.append(gi)
                mMeanList.append(gi)
            }
        }
        
        mWordList.shuffle()
        mMeanList.shuffle()
        
        mTableWord.reloadData()
        mTableMean.reloadData()
        
        left = 3
        score = 0
        
        updateScore()
        updateLeft()
    }
    
    func updateScore() {
        mScore.text = String(score)
    }
    
    func updateLeft(){
        
        if (left < 0) {
            showResult()
            return
        }
        
        if (left == 0) {
            mLeftIconm.image = UIImage(named: "quiz_l0")
        }
        else if (left == 1) {
            mLeftIconm.image = UIImage(named: "quiz_l1")
        }
        else if (left == 2) {
            mLeftIconm.image = UIImage(named: "quiz_l2")
        }
        else {
            mLeftIconm.image = UIImage(named: "quiz_l5")
        }
        
        mLeft.text = String(left)
    }
    
    func showResult() {
        mGameScoreResult.text = String(score)
        if (left < 0) {
            mGameDialogTitle.text = "GAME OVER"
        }
        else {
            mGameDialogTitle.text = "YOU WIN"
        }
        
        UIView.animate(withDuration: 0.2, animations: {
            self.mGameResultContainer.isHidden = false
            self.mGameResultContainer.alpha = 1
        });
    }
    
    @IBAction func onExit(_ sender: Any) {
        UIView.animate(withDuration: 0.2, animations: {
            self.mGameResultContainer.alpha = 0
        }, completion: { (valuee : Bool) in
            self.mGameResultContainer.isHidden = true
            
            self.navigationController?.popViewController(animated: true)
        })
    }
    
    @IBAction func onTryAgain(_ sender: Any) {
        initData()
        UIView.animate(withDuration: 0.2, animations: {
            self.mGameResultContainer.alpha = 0
        }, completion: { (valuee : Bool) in
            self.mGameResultContainer.isHidden = true
        })
    }
}

extension GameViewController : UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (tableView == mTableWord) {
            return mWordList.count
        }
        else {
            return mMeanList.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! GameItemCell
        
        var data : GameItem
        
        if (tableView == mTableWord) {
            data = mWordList[indexPath.row]
            cell.mText.text = data.word
        }
        else {
            data = mMeanList[indexPath.row]
            cell.mText.text = data.mean
        }
        
        cell.mData = data
        cell.mContainer.layer.cornerRadius = 5
        cell.mContainer.layer.masksToBounds = true
        
        cell.selectionStyle = .none
        
        if (data.isSelected == true) {
            cell.mContainer.backgroundColor = UIColor.lightGray
        }
        else {
            cell.mContainer.backgroundColor = UIColor.white
        }
        
        return cell
    }
}


extension GameViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if (tableView == mTableWord) {
            if (isSelectedWord == 1) {
                return
            }
            
            for i in 0 ..< mWordList.count {
                mWordList[i].isSelected = false
            }
            
            if (mSelectedGameItem != nil) {
                let currentGameItem = mWordList[indexPath.row]
                
                if (mSelectedGameItem.word == currentGameItem.word) {
                    //right
                    remove(item: currentGameItem)
                    isSelectedWord = -1
                }
                else {
                    //wrong - clear
                    mSelectedGameItem = nil
                    for i in 0 ..< mMeanList.count {
                        mMeanList[i].isSelected = false
                    }
                    
                    left = left - 1
                    updateLeft()
                    isSelectedWord = -1
                }
            }
            else {
                mSelectedGameItem = mWordList[indexPath.row]
                mWordList[indexPath.row].isSelected = true
                isSelectedWord = 1
            }
        }
        else {
            if (isSelectedWord == 0) {
                return
            }
            
            for i in 0 ..< mMeanList.count {
                mMeanList[i].isSelected = false
            }
            
            if (mSelectedGameItem != nil) {
                let currentGameItem = mMeanList[indexPath.row]
                
                if (mSelectedGameItem.word == currentGameItem.word) {
                    //right
                    remove(item: currentGameItem)
                    isSelectedWord = -1
                }
                else {
                    //wrong - clear
                    mSelectedGameItem = nil
                    for i in 0 ..< mWordList.count {
                        mWordList[i].isSelected = false
                    }
                    
                    left = left - 1
                    updateLeft()
                    isSelectedWord = -1
                }
            }
            else {
                mSelectedGameItem = mMeanList[indexPath.row]
                mMeanList[indexPath.row].isSelected = true
                isSelectedWord = 0
            }
        }
        
        mTableWord.reloadData()
        mTableMean.reloadData()
    }
    
    func remove(item : GameItem) {
        let wcount = mWordList.count
        let mcount = mMeanList.count
        
        for i in 0 ..< wcount {
            if (mWordList[i].word == item.word) {
                mWordList.remove(at: i)
                break
            }
        }
        
        for i in 0 ..< mcount {
            if (mMeanList[i].word == item.word) {
                mMeanList.remove(at: i)
                break
            }
        }
        
        mSelectedGameItem = nil
        score = score + 10
        updateScore()
        
        if (mWordList.count == 0) {
            showResult()
        }
    }
}

extension MutableCollection where Indices.Iterator.Element == Index {
    /// Shuffles the contents of this collection.
    mutating func shuffle() {
        let c = count
        guard c > 1 else { return }
        
        for (firstUnshuffled , unshuffledCount) in zip(indices, stride(from: c, to: 1, by: -1)) {
            let d: IndexDistance = numericCast(arc4random_uniform(numericCast(unshuffledCount)))
            guard d != 0 else { continue }
            let i = index(firstUnshuffled, offsetBy: d)
            swap(&self[firstUnshuffled], &self[i])
        }
    }
}

extension Sequence {
    /// Returns an array with the contents of this sequence, shuffled.
    func shuffled() -> [Iterator.Element] {
        var result = Array(self)
        result.shuffle()
        return result
    }
}
