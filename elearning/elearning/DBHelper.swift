import SQLite

protocol ImportCallback {
    func onSuccess()
}

class DBHelper : NSObject {
    var db : Connection?
    
    let type_table : Table = Table("type")
    let type_id : Expression<Int64> = Expression<Int64>("id")
    let type_value : Expression<String>? = Expression<String>("value")
    
    let language_table : Table = Table("language")
    let language_id : Expression<Int64> = Expression<Int64>("id")
    let language_value : Expression<String>? = Expression<String>("value")
    let language_language : Expression<String>? = Expression<String>("language")
    
    let lesson_table : Table = Table("lesson")
    let lesson_id : Expression<Int64> = Expression<Int64>("id")
    let lesson_type : Expression<Int64> = Expression<Int64>("type")
    let lesson_language : Expression<String> = Expression<String>("language")
    let lesson_title : Expression<String>? = Expression<String>("title")
    let lesson_audio : Expression<String>? = Expression<String>("audio")
    let lesson_audio2 : Expression<String>? = Expression<String>("audio2")
    let lesson_image : Expression<String>? = Expression<String>("image")
    let lesson_score : Expression<Int64> = Expression<Int64>("score")
    let lesson_needed_score : Expression<Int64> = Expression<Int64>("needed_score")
    let lesson_total_score : Expression<Int64> = Expression<Int64>("total_score")
    let lesson_lock : Expression<Int64> = Expression<Int64>("lock")
    
    let lesson_detail_table : Table = Table("lesson_detail")
    let lesson_detail_id : Expression<Int64> = Expression<Int64>("id")
    let lesson_detail_lesson : Expression<Int64> = Expression<Int64>("lesson")
    let lesson_detail_title : Expression<String>? = Expression<String>("title")
    let lesson_detail_meaning : Expression<String>? = Expression<String>("meaning")
    let lesson_detail_audio : Expression<String>? = Expression<String>("audio")
    let lesson_detail_audio2 : Expression<String>? = Expression<String>("audio2")
    let lesson_detail_score : Expression<Int64> = Expression<Int64>("score")
    let lesson_detail_timesave : Expression<Int64> = Expression<Int64>("timesave")
    
    override init() {}
    
    var importCallback : ImportCallback!
    var isUsingDatabase : Bool = true
    var _lessonDetails : [LessonDetail] = []
    
    func initData() {
        do {
            db = try Connection(Utils.getPath(fileName: "elearning.sqlite3"))
        }
        catch {}
    }
    
    func addLanguage(languages : [Language]) {
        for language in languages {
            let insert = language_table.insert(language_value! <- language.value,
                                               language_language! <- language.language)
            do {
                try db?.run(insert)
            }
            catch {}
        }
    }
    
    func addLesson(lessons : [Lesson]) {
        let queue = DispatchQueue(label: "add lesson")
        queue.async {
            for (index, lesson) in lessons.enumerated() {
                let insert = self.lesson_table.insert(self.lesson_id <- Int64(lesson.id),
                                                 self.lesson_type <- Int64(lesson.type),
                                                 self.lesson_language <- lesson.language,
                                                 self.lesson_title! <- lesson.title,
                                                 self.lesson_audio! <- lesson.audio,
                                                 self.lesson_audio2! <- lesson.audio2,
                                                 self.lesson_image! <- lesson.image,
                                                 self.lesson_score <- Int64(lesson.score),
                                                 self.lesson_needed_score <- Int64(lesson.needed_score),
                                                 self.lesson_total_score <- Int64(lesson.total_score),
                                                 self.lesson_lock <- Int64(lesson.lock))
                do {
                    try self.db?.run(insert)
                    if (index == (lessons.count - 1)) {
                        self.importCallback.onSuccess()
                    }
                }
                catch {}
            }
        }
    }
    
    func addLessonDetail(lessonDetails : [LessonDetail]) {
        _lessonDetails = lessonDetails
        let queue = DispatchQueue(label: "add lesson detail")
        queue.async {
            for (index, lessonDetail) in lessonDetails.enumerated() {
                let insert = self.lesson_detail_table.insert(self.lesson_detail_id <- Int64(lessonDetail.id),
                                                        self.lesson_detail_lesson <- Int64(lessonDetail.lesson),
                                                        self.lesson_detail_title! <- lessonDetail.title,
                                                        self.lesson_detail_meaning! <- lessonDetail.meaning,
                                                        self.lesson_detail_audio! <- lessonDetail.audio,
                                                        self.lesson_detail_audio2! <- lessonDetail.audio2,
                                                        self.lesson_detail_score <- Int64(lessonDetail.score),
                                                        self.lesson_detail_timesave <- Int64(lessonDetail.timesave))
                do {
                    try self.db?.run(insert)
                    if (index == (lessonDetails.count - 1)) {
                        self.isUsingDatabase = true
                    }
                }
                catch {}
            }
        }
    }
    
    func getLessonByLanguageAndType(lang : String, type : Int64) -> [Lesson] {
        var returnValue : [Lesson] = []
        let condition = lesson_table.filter(lesson_language == lang).filter(lesson_type == type)
        do {
            let lessons = try db?.prepare(condition)
            
            for lesson in lessons! {
                let id = Int(lesson[lesson_id])
                let type = Int(lesson[lesson_type])
                let language = lesson[lesson_language]
                let title = lesson[lesson_title!]
                let audio = lesson[lesson_audio!]
                let audio2 = lesson[lesson_audio2!]
                let image = lesson[lesson_image!]
                let score = Int(lesson[lesson_score])
                let needed_score = Int(lesson[lesson_needed_score])
                let total_score = Int(lesson[lesson_total_score])
                let lock = lesson[lesson_lock]
                
                let les = Lesson(id: id, type: type, language: language,
                                 title: title, audio: audio, audio2: audio2,
                                 image: image, score: score, needed_score: needed_score,
                                 total_score: total_score, lock: Int(lock))
                returnValue.append(les)
            }
            
        }
        catch {}
        
        return returnValue
    }
    
    func getLessonByID(id : Int64) -> Lesson {
        var returnValue : Lesson!
        let condition = lesson_table.filter(lesson_id == id)
        do {
            let lessons = try db?.prepare(condition)
            
            for lesson in lessons! {
                let id = Int(lesson[lesson_id])
                let type = Int(lesson[lesson_type])
                let language = lesson[lesson_language]
                let title = lesson[lesson_title!]
                let audio = lesson[lesson_audio!]
                let audio2 = lesson[lesson_audio2!]
                let image = lesson[lesson_image!]
                let score = Int(lesson[lesson_score])
                let needed_score = Int(lesson[lesson_needed_score])
                let total_score = Int(lesson[lesson_total_score])
                let lock = lesson[lesson_lock]
                
                let les = Lesson(id: id, type: type, language: language,
                                 title: title, audio: audio, audio2: audio2,
                                 image: image, score: score, needed_score: needed_score,
                                 total_score: total_score, lock: Int(lock))
                returnValue = les
            }
            
        }
        catch {}
        
        return returnValue
    }
    
    func getLessonDetailByLessonID(lesson : Int64) -> [LessonDetail] {
        if (isUsingDatabase == true) {
            var returnValue : [LessonDetail] = []
            let condition = lesson_detail_table.filter(lesson_detail_lesson == lesson)
            do {
                let lessonDetails = try db?.prepare(condition)
                for lessonDetail in lessonDetails! {
                    let id = Int(lessonDetail[lesson_detail_id])
                    let lesson = Int(lessonDetail[lesson_detail_lesson])
                    let title = lessonDetail[lesson_detail_title!]
                    let meaning = lessonDetail[lesson_detail_meaning!]
                    let audio = lessonDetail[lesson_detail_audio!]
                    let audio2 = lessonDetail[lesson_detail_audio2!]
                    let score = Int(lessonDetail[lesson_detail_score])
                    let timesave = Int(lessonDetail[lesson_detail_timesave])
                    
                    
                    let lesDetail = LessonDetail(_id: id, _lesson: lesson,
                                                 _title: title, _meaning: meaning,
                                                 _audio: audio, _audio2: audio2,
                                                 _score: score, _timesave: timesave)
                    returnValue.append(lesDetail)
                }
            }
            catch {}
            
            return returnValue
        }
        else {
            var returnValue : [LessonDetail] = []
            for lessonDetail in self._lessonDetails {
                if (lessonDetail.lesson == Int(lesson)) {
                    returnValue.append(lessonDetail)
                }
            }
            
            return returnValue
        }
    }
    
    func getLessonDetailByID(id : Int64) -> LessonDetail {
        var returnValue : LessonDetail!
        let condition = lesson_detail_table.filter(lesson_detail_id == id)
        do {
            let lessonDetails = try db?.prepare(condition)
            for lessonDetail in lessonDetails! {
                let id = Int(lessonDetail[lesson_detail_id])
                let lesson = Int(lessonDetail[lesson_detail_lesson])
                let title = lessonDetail[lesson_detail_title!]
                let meaning = lessonDetail[lesson_detail_meaning!]
                let audio = lessonDetail[lesson_detail_audio!]
                let audio2 = lessonDetail[lesson_detail_audio2!]
                let score = Int(lessonDetail[lesson_detail_score])
                let timesave = Int(lessonDetail[lesson_detail_timesave])
                
                
                let lesDetail = LessonDetail(_id: id, _lesson: lesson,
                                             _title: title, _meaning: meaning,
                                             _audio: audio, _audio2: audio2,
                                             _score: score, _timesave: timesave)
                returnValue = lesDetail
            }
        }
        catch {}
        
        return returnValue
    }
    
    func getMyWord() -> [LessonDetail] {
        var returnValue : [LessonDetail] = []
        let condition = lesson_detail_table.filter(lesson_detail_timesave > 0).order(lesson_detail_timesave.desc)
        do {
            let lessonDetails = try db?.prepare(condition)
            for lessonDetail in lessonDetails! {
                let id = Int(lessonDetail[lesson_detail_id])
                let lesson = Int(lessonDetail[lesson_detail_lesson])
                let title = lessonDetail[lesson_detail_title!]
                let meaning = lessonDetail[lesson_detail_meaning!]
                let audio = lessonDetail[lesson_detail_audio!]
                let audio2 = lessonDetail[lesson_detail_audio2!]
                let score = Int(lessonDetail[lesson_detail_score])
                let timesave = Int(lessonDetail[lesson_detail_timesave])
                
                
                let lesDetail = LessonDetail(_id: id, _lesson: lesson,
                                             _title: title, _meaning: meaning,
                                             _audio: audio, _audio2: audio2,
                                             _score: score, _timesave: timesave)
                returnValue.append(lesDetail)
            }
        }
        catch {}
        
        return returnValue
    }
    
    func addWord(word : String, mean : String) {
        let timesave = Date().timeIntervalSince1970
        var lds : [LessonDetail] = []
        let ld = LessonDetail(_id: 0, _lesson: 0, _title: word, _meaning: mean, _audio: "", _audio2: "", _score: 0, _timesave: Int(timesave))
        lds.append(ld)
        
        addLessonDetail(lessonDetails: lds)
    }
    
    func updateScore(id : Int64, score : Int64) {
        do {
            var update = lesson_detail_table.filter(lesson_detail_id == id).update(lesson_detail_score <- score)
            try db?.run(update)
            
            //get lesson detail
            let lesDetail = getLessonDetailByID(id: id)
            //get id of lesson
            let lesson_id = lesDetail.lesson
            //get lesson
            let les = getLessonByID(id: Int64(lesson_id))
            //get score of lesson
            var les_score = les.score
            les_score += Int(score)
            //update score of lesson
            update = lesson_table.filter(self.lesson_id == Int64(lesson_id)).update(self.lesson_score <- Int64(les_score))
            try db?.run(update)
        }
        catch {}
    }
    
    func updateScoreByTimeSave(timeSave : Int64, score : Int64) {
        do {
            let update = lesson_detail_table.filter(lesson_detail_timesave == timeSave).update(lesson_detail_score <- score)
            try db?.run(update)
        }
        catch {}
    }
    
    func updateMyWord(id : Int64, timeinterval : Int64) {
        do {
            let update = lesson_detail_table.filter(lesson_detail_id == id).update(lesson_detail_timesave <- timeinterval)
            try db?.run(update)
        }
        catch {}
    }
}
