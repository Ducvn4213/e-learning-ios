struct Lesson {
    var id: Int
    var type : Int
    var language : String
    var title : String
    var audio : String
    var audio2 : String
    var image : String
    var score : Int
    var needed_score : Int
    var total_score : Int
    var lock : Int
}
