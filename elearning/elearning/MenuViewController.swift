import UIKit
import SideMenu

class MenuViewController : UITableViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func onChangeNativeLanguage(_ sender: Any) {
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let mainViewController = storyBoard.instantiateViewController(withIdentifier: "language_select") as! LanguageSelectViewController
        mainViewController.mNeedDissmis = true
        self.present(mainViewController, animated: true, completion: nil)
       
//        let alertController = UIAlertController(title: "Change your native language", message: "Are you sure?", preferredStyle: .alert)
//        
//        let action = UIAlertAction(title: "Yes", style: .default) { action in
//            
//        }
//        alertController.addAction(action)
//        
//        let cc_action = UIAlertAction(title: "Cancel", style: .cancel) { action in }
//        alertController.addAction(cc_action)
//        
//        self.present(alertController, animated: true)
    }
    
    @IBAction func onRating(_ sender: Any) {
        if let url = NSURL(string: "itms-apps://itunes.apple.com/app/id959379869"){
            UIApplication.shared.open(url as URL, options: [:], completionHandler: nil)
        }
    }
    
    @IBAction func onOtherApps(_ sender: Any) {
        if let url = NSURL(string: "itms-apps://itunes.apple.com/us/developer/red-wolf-studio/id953326323"){
            UIApplication.shared.open(url as URL, options: [:], completionHandler: nil)
        }
    }
    
    @IBAction func onShare(_ sender: Any) {
        let activityViewController = UIActivityViewController(activityItems: ["Learn English" as String], applicationActivities: nil)
        present(activityViewController, animated: true, completion: {})
    }
    
    @IBAction func onFanpage(_ sender: Any) {
        if let url = NSURL(string: "https://www.facebook.com/sovio.net/?fref=ts"){
            UIApplication.shared.open(url as URL, options: [:], completionHandler: nil)
        }
    }
    
    @IBAction func onGroup(_ sender: Any) {
        if let url = NSURL(string: "https://www.facebook.com/groups/982274605194296/"){
            UIApplication.shared.open(url as URL, options: [:], completionHandler: nil)
        }
    }
}
