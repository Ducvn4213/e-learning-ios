import Foundation
import UIKit
import Alamofire
import AVFoundation
import Speech

protocol DownloadAndSaveLanguageCallback {
    func onSuccess()
}

protocol DownloadLanguageCallback {
    func onSuccess(data : [LanguageResponseItem])
}

class ELService : NSObject, ImportCallback {
    
    let speechRecognizer = SFSpeechRecognizer(locale: Locale.init(identifier: "en-US"))!
    var recognitionRequest: SFSpeechAudioBufferRecognitionRequest?
    var recognitionTask: SFSpeechRecognitionTask?
    let audioEngine = AVAudioEngine()
    
    var mDBHelper : DBHelper!
    var downloadAndSaveDataCallback : DownloadAndSaveLanguageCallback!
    var downloadLanguageCallback : DownloadLanguageCallback!
    var speechToTextCallback : SpeechToTextCallback!
    
    var playerItem : AVPlayerItem?
    var player : AVPlayer?
    
    //the original size
    var originalTitleWidth : CGFloat!
    var originalTitle2Width : CGFloat!
    var originalContainerHeight: CGFloat!
    
    var originalTitleWidth2 : CGFloat!
    var originalTitle2Width2 : CGFloat!
    var originalContainerHeight2: CGFloat!
    
    //for game
    var gameItems : [LessionItem]!
    
    var viewControllerBeforeGoToLanguage : UIViewController!
    
    override init() {
    }
    
    func initService() {
        let language = self.getSavedLanguage()
        if (language == "") {
            Utils.copyFile(fileName: "elearning.sqlite3")
        }
        mDBHelper = DBHelper()
        mDBHelper.initData()
    }
    
    func playURL(url: String) {
        let _url = URL(string: url)
        playerItem = AVPlayerItem(url: _url!)
        player = AVPlayer(playerItem: playerItem!)
        player?.play()
    }
    
    func isDownloaded(code : String) -> Bool {
        let langList = getSavedLanguageList()
        if (langList.contains(code)) {
            return true
        }
        
        return false
    }
    
    func saveTimeNoti(time : String) {
        UserDefaults.standard.set(time, forKey: EL.SP_TIME_NOTI)
    }
    
    func saveTimeNotiEnable(enable : Bool) {
        var enableString = "false"
        
        if (enable == true) {
            enableString = "true"
        }
        
        UserDefaults.standard.set(enableString, forKey: EL.SP_TIME_NOTI_ENABLE)
    }
    
    func saveLanguage(language : String) {
        UserDefaults.standard.set(language, forKey: EL.SP_LANGUAGE)
        
        var langList = getSavedLanguageList()
        langList += language + "/"
        UserDefaults.standard.set(langList, forKey: EL.SP_LANGUAGE_LIST)
    }
    
    func getSavedTimeNoti() -> String {
        let time = UserDefaults.standard.string(forKey: EL.SP_TIME_NOTI)
        if (time == nil) {
            return ""
        }
        
        return time!
    }
    
    func getTimeNotiEnable() -> Bool {
        let enable = UserDefaults.standard.string(forKey: EL.SP_TIME_NOTI_ENABLE)
        if (enable == nil || enable == "true") {
            return true
        }
        
        return false
    }
    
    func getSavedLanguage() -> String {
        let language = UserDefaults.standard.string(forKey: EL.SP_LANGUAGE)
        if (language == nil) {
            return ""
        }
        
        return language!
    }
    
    func getSavedLanguageList() -> String {
        let language = UserDefaults.standard.string(forKey: EL.SP_LANGUAGE_LIST)
        if (language == nil) {
            return ""
        }
        
        return language!
    }
    
    func downloadLanguage() {
        let parameters: Parameters = ["_request" : "getlanguage"]
        Alamofire.request(EL.WEB_API, method: .post, parameters: parameters).responseJASON { response in
            if let json = response.result.value {
                let results = json.map(LanguageResponseItem.init)
                self.downloadLanguageCallback.onSuccess(data: results)
            }
        }
    }
    
    func dowloadDataWithLanguage(language : String) {
        let parameters: Parameters = ["_request" : "getdata",
                                      "_lang" : language]
        
        Alamofire.request(EL.WEB_API, method: .post, parameters: parameters).responseJASON { response in
            if let json = response.result.value {
                let results = json.map(LessonResponseItem.init)
                self.writeToDB(data: results)
            }
        }
        
    }
    
    func writeToDB(data : [LessonResponseItem]) {
        var lessons : [Lesson] = []
        var lessonDetails : [LessonDetail] = []
        
        for lessonItem in data {
            let score = lessonItem.detail.count * 5
            let needed = (score * 7) / 10
            
            let lock = 0
            
            let lesson = Lesson(id: lessonItem.id, type: lessonItem.type, language: lessonItem.language,
                                title: lessonItem.title, audio: lessonItem.audio, audio2: lessonItem.audio2,
                                image: lessonItem.image, score: 0, needed_score: needed, total_score: score, lock: lock)
            
            lessons.append(lesson)
            
            for lessonDetail in lessonItem.detail {
                let lesDetail = LessonDetail(_id: lessonDetail.id, _lesson: lessonItem.id, _title: lessonDetail.title,
                                             _meaning: lessonDetail.meaning, _audio: lessonDetail.audio, _audio2: lessonDetail.audio2,
                                             _score: 0, _timesave: 0)
                
                lessonDetails.append(lesDetail)
            }
        }
        
        mDBHelper.isUsingDatabase = false
        mDBHelper.importCallback = self
        mDBHelper.addLesson(lessons: lessons)
        mDBHelper.addLessonDetail(lessonDetails: lessonDetails)
    }
    
    func onSuccess() {
        if (self.downloadAndSaveDataCallback != nil) {
            self.downloadAndSaveDataCallback.onSuccess()
        }
    }
    
    func addWord(word : String, mean : String) {
        mDBHelper.addWord(word: word, mean: mean)
    }
    
    func getConversationLesson() -> [LessionListItem] {
        return getLessonByType(type: 1)
    }
    
    func getVocabularyLesson() -> [LessionListItem] {
        return getLessonByType(type: 2)
    }
    
    func getPhraseLesson() -> [LessionListItem] {
        return getLessonByType(type: 3)
    }
    
    func getLessonDetailByLessonID(lesson : Int) -> [LessionItem] {
        var returnValue : [LessionItem] = []
        
        let lessonDetails = mDBHelper.getLessonDetailByLessonID(lesson: Int64(lesson))
        for lessonDetail in lessonDetails {
            let lesDetail = LessionItem(coplap_height: 85, margin: 0, height: 85, id : lessonDetail.id, sentence: lessonDetail.title, meaning: lessonDetail.meaning, score: lessonDetail.score, audio: lessonDetail.audio, audio2: lessonDetail.audio2, timesave: lessonDetail.timesave)
            returnValue.append(lesDetail)
        }
        
        return returnValue
    }
    
    func getMyWord() -> [LessionItem] {
        var returnValue : [LessionItem] = []
        
        let lessonDetails = mDBHelper.getMyWord()
        for lessonDetail in lessonDetails {
            let lesDetail = LessionItem(coplap_height: 85, margin: 0, height: 85, id : lessonDetail.id, sentence: lessonDetail.title, meaning: lessonDetail.meaning, score: lessonDetail.score, audio: lessonDetail.audio, audio2: lessonDetail.audio2, timesave: lessonDetail.timesave)
            returnValue.append(lesDetail)
        }
        
        return returnValue
    }
    
    func getLessonByType(type : Int) -> [LessionListItem] {
        var returnValue : [LessionListItem] = []
        let lang = self.getSavedLanguage()
        let lessons = mDBHelper.getLessonByLanguageAndType(lang: lang, type: Int64(type))
        for lesson in lessons {
            let les = LessionListItem(id : lesson.id, score: lesson.score, max_score: lesson.total_score, needed_score: lesson.needed_score, image: lesson.image, title: lesson.title, lock: lesson.lock, audio: lesson.audio)
            returnValue.append(les)
        }
        
        return returnValue
    }
    
    func updateScore(id : Int, score : Int) {
        mDBHelper.updateScore(id: Int64(id), score: Int64(score))
    }
    
    func updateScoreByTimeSave(timesave : Int, score : Int) {
        mDBHelper.updateScoreByTimeSave(timeSave: Int64(timesave), score: Int64(score))
    }
    
    func addToMyWord(id : Int) {
        let now = Date()
        let timeinterval = now.timeIntervalSince1970
        mDBHelper.updateMyWord(id: Int64(id), timeinterval: Int64(timeinterval))
    }
    
    func removeFromMyWord(id : Int) {
        mDBHelper.updateMyWord(id: Int64(id), timeinterval: 0)
    }
}

protocol SpeechToTextCallback {
    func onSuccess(text: String)
    func onFail()
}

extension ELService : SFSpeechRecognizerDelegate {

    func startSTT() {
        if audioEngine.isRunning {
            audioEngine.stop()
            recognitionRequest?.endAudio()
        }
        else {
            doSTT()
        }
    }
    
    func stopSTT() {
        if audioEngine.isRunning {
            audioEngine.stop()
            recognitionRequest?.endAudio()
        }
        
        recognitionTask?.cancel()
        recognitionTask = nil
    }
    
    func doSTT() {
        
        
        if recognitionTask != nil {
            recognitionTask?.cancel()
            recognitionTask = nil
        }
        
        let audioSession = AVAudioSession.sharedInstance()
        do {
            try audioSession.setCategory(AVAudioSessionCategoryRecord)
            try audioSession.setMode(AVAudioSessionModeMeasurement)
            try audioSession.setActive(true, with: .notifyOthersOnDeactivation)
        } catch {
            print("audioSession properties weren't set because of an error.")
        }
        
        recognitionRequest = SFSpeechAudioBufferRecognitionRequest()
        
        guard let inputNode = audioEngine.inputNode else {
            fatalError("Audio engine has no input node")
        }
        
        guard let recognitionRequest = recognitionRequest else {
            fatalError("Unable to create an SFSpeechAudioBufferRecognitionRequest object")
        }
        
        recognitionRequest.shouldReportPartialResults = true
        
        recognitionTask = speechRecognizer.recognitionTask(with: recognitionRequest, resultHandler: { (result, error) in
            
            var isFinal = false
            
            if result != nil {
                self.speechToTextCallback.onSuccess(text: (result?.bestTranscription.formattedString)!)
                isFinal = (result?.isFinal)!
            }
            
            if error != nil || isFinal {
                self.audioEngine.stop()
                inputNode.removeTap(onBus: 0)
                
                self.recognitionRequest = nil
                self.recognitionTask = nil
            }
        })
        
        let recordingFormat = inputNode.outputFormat(forBus: 0)
        inputNode.installTap(onBus: 0, bufferSize: 1024, format: recordingFormat) { (buffer, when) in
            self.recognitionRequest?.append(buffer)
        }
        
        audioEngine.prepare()
        
        do {
            try audioEngine.start()
        } catch {
            print("audioEngine couldn't start because of an error.")
        }
    }
}
