import UIKit

class WordFlashCardGameViewController : UIViewController {

    @IBOutlet weak var mFlashCardView: UIView!
    @IBOutlet weak var mFlashCard1: UIView!
    @IBOutlet weak var mFlashCard2: UIView!
    
    @IBOutlet weak var mTitle2: UILabel!
    @IBOutlet weak var mStatus2: UILabel!
    @IBOutlet weak var mMeaning: UILabel!
    @IBOutlet weak var mStatus1: UILabel!
    @IBOutlet weak var mTitle1: UILabel!
    
    @IBOutlet weak var mProgressContainer: UIView!
    @IBOutlet weak var mProgress1: UIView!
    @IBOutlet weak var mProgress2: UIView!
    @IBOutlet weak var mProgress3: UIView!
    @IBOutlet weak var mProgressText1: UILabel!
    @IBOutlet weak var mProgressText2: UILabel!
    @IBOutlet weak var mProgressText3: UILabel!
    
    var mData : [LessionItem] = []
    
    var isFlipped : Bool = false
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    var mService : ELService!
    
    var mCurrentLession : LessionItem!
    var mCurrentIndex : Int!
    
    var mBlock1 : [LessionItem] = []
    var mBlock2 : [LessionItem] = []
    var mBlock3 : [LessionItem] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mService = appDelegate.getELService()
        
        initData()
        
        mCurrentIndex = 0
        loadAnItem()
    }
    
    func initData() {
        for gi in mService.gameItems {
            if (gi.meaning.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines) != "") {
                self.mData.append(gi)
            }
        }
        
        mData.shuffle()
    }
    
    func loadAnItem() {
        mCurrentLession = mData[mCurrentIndex]
        
        mTitle1.text = mCurrentLession.sentence
        mTitle2.text = mCurrentLession.sentence
        mMeaning.text = mCurrentLession.meaning
        
        mStatus1.text = getStatus()
        mStatus2.text = getStatus()
        
        mProgressText1.text = "You have mastered " + String(mBlock1.count) + " out of " + String(mData.count) + " words"
        mProgressText2.text = "You are reviewing " + String(mBlock2.count) + " out of " + String(mData.count) + " words"
        mProgressText3.text = "You are learning " + String(mBlock3.count) + " out of " + String(mData.count) + " words"
        
        let percentProgress1 = CGFloat(mBlock1.count) / CGFloat(mData.count)
        let percentProgress2 = CGFloat(mBlock2.count) / CGFloat(mData.count)
        let percentProgress3 = CGFloat(mBlock3.count) / CGFloat(mData.count)
        
        
        let totalWidth = mProgressContainer.frame.width
        mProgress1.frame.size.width = totalWidth * percentProgress1
        mProgress2.frame.size.width = totalWidth * percentProgress2
        mProgress3.frame.size.width = totalWidth * percentProgress3
    }
    
    @IBAction func onDefinition(_ sender: Any) {
        UIView.transition(with: mFlashCardView!, duration: 0.5, options:.transitionFlipFromRight, animations: { () -> Void in
            
            if (self.isFlipped) {
                self.mFlashCard1.isHidden = false
                self.mFlashCard2.isHidden = true
            }
            else {
                self.mFlashCard1.isHidden = true
                self.mFlashCard2.isHidden = false
            }
        }, completion: { (Bool) -> Void in
            self.isFlipped = true
        })
    }
    
    @IBAction func onDontKnow(_ sender: Any) {
        onNext(knew: false)
        UIView.transition(with: mFlashCardView!, duration: 0.5, options:.transitionFlipFromRight, animations: { () -> Void in
            
            if (self.isFlipped) {
                self.mFlashCard1.isHidden = false
                self.mFlashCard2.isHidden = true
            }
            else {
                self.mFlashCard1.isHidden = true
                self.mFlashCard2.isHidden = false
            }
        }, completion: { (Bool) -> Void in
            self.isFlipped = false
        })
    }
    
    @IBAction func onKnown(_ sender: Any) {
        onNext(knew: true)
        UIView.transition(with: mFlashCardView!, duration: 0.5, options:.transitionFlipFromRight, animations: { () -> Void in
            
            if (self.isFlipped) {
                self.mFlashCard1.isHidden = false
                self.mFlashCard2.isHidden = true
            }
            else {
                self.mFlashCard1.isHidden = true
                self.mFlashCard2.isHidden = false
            }
        }, completion: { (Bool) -> Void in
            self.isFlipped = false
        })
    }
    
    func onNext(knew : Bool) {
        if (knew) {
            if (isContain(item: mCurrentLession, block: mBlock3)) {
                let block3Index = indexOf(item: mCurrentLession, block: mBlock3)
                if (block3Index > -1) {
                    mBlock3.remove(at: block3Index)
                }
                
                mBlock2.append(mCurrentLession)
            }
            else {
                let block2Index = indexOf(item: mCurrentLession, block: mBlock2)
                
                if (block2Index > -1) {
                    mBlock2.remove(at: block2Index)
                }
                
                if (mStatus1.text != "mastered") {
                    mBlock1.append(mCurrentLession)
                }
            }
        }
        else {
            //remove from all of reviewing and mastered
            let block1Index = indexOf(item: mCurrentLession, block: mBlock1)
            let block2Index = indexOf(item: mCurrentLession, block: mBlock2)
            let block3Index = indexOf(item: mCurrentLession, block: mBlock3)
            
            if (block1Index > -1) {
                mBlock1.remove(at: block1Index)
            }
            if (block2Index > -1) {
                mBlock2.remove(at: block2Index)
            }
            if (block3Index > -1) {
                mBlock3.remove(at: block3Index)
            }
            mBlock3.append(mCurrentLession)
        }
        
        
        mCurrentIndex = mCurrentIndex + 1
        if (mCurrentIndex == mData.count) {
            mCurrentIndex = 0
        }
        
        loadAnItem()
    }
    
    @IBAction func onSpeak(_ sender: Any) {
        mService.playURL(url: mCurrentLession.audio)
    }
    
    func getStatus() -> String {
        if (isContain(item: mCurrentLession, block: mBlock1)) {
            return "mastered"
        }
        else if (isContain(item: mCurrentLession, block: mBlock2)) {
            return "reviewing"
        }
        else if (isContain(item: mCurrentLession, block: mBlock3)) {
            return "learning"
        }
        else {
            return "new word"
        }
    }
    
    func isContain(item : LessionItem, block : [LessionItem]) -> Bool {
        for b in block {
            if (b.sentence == item.sentence) {
                return true
            }
        }
        
        return false
    }
    
    func indexOf(item : LessionItem, block : [LessionItem]) -> Int {
        for (i, b) in block.enumerated() {
            if (b.sentence == item.sentence) {
                return i
            }
        }
        
        return -1
    }
}
