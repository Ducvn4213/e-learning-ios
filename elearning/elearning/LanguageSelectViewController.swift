import UIKit
import SideMenu

class LanguageSelectViewController : UIViewController {
    
    @IBOutlet weak var mTableView: UITableView!
    @IBOutlet weak var mLoadingView: UIView!
    
    var mData : [String] = []
    var mDataCode : [String] = []
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    var mService : ELService!
    
    var mNeedDissmis : Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mService = appDelegate.getELService()
        mService.downloadAndSaveDataCallback = self
        mService.downloadLanguageCallback = self
        
        self.mLoadingView.isHidden = true
        self.mLoadingView.alpha = 0
        
        loadData()
    }
    
    func loadData() {
        mService.downloadLanguage()
        //mData = EL.LS_LANGUAGE
        //mDataCode = EL.LS_LANGUAGE_CODE
    }
    
    @IBAction func onBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}

extension LanguageSelectViewController : DownloadLanguageCallback {
    func onSuccess(data: [LanguageResponseItem]) {
        for lang in data {
            mData.append(lang.language)
            mDataCode.append(lang.value)
        }
        
        mTableView.reloadData()
    }
}

extension LanguageSelectViewController : DownloadAndSaveLanguageCallback {
    
    func onSuccess() {
        if (mNeedDissmis) {
            mService.viewControllerBeforeGoToLanguage.viewDidAppear(true)
            self.dismiss(animated: true, completion: nil)
            return
        }
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let mainViewController = storyBoard.instantiateViewController(withIdentifier: "main")
        self.present(mainViewController, animated: true, completion: nil)
    }
}

extension LanguageSelectViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return mData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! LanguageItemCell
        
        cell.title.text = mData[indexPath.row]
        
        return cell
    }
}


extension LanguageSelectViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let lang_code = self.mDataCode[indexPath.row]
        if (mService.isDownloaded(code: lang_code)) {
            self.mService.saveLanguage(language: lang_code)
            onSuccess()
            return
        }
        
        let alertController = UIAlertController(title: "Notice", message: "You can change your native language in setting", preferredStyle: .alert)
        
        let action = UIAlertAction(title: "OK", style: .cancel) { action in
            
            UIView.animate(withDuration: 0.2, animations: {
                self.mLoadingView.isHidden = false
                self.mLoadingView.alpha = 0.85
            })
            
            self.mService.saveLanguage(language: lang_code)
            self.mService.dowloadDataWithLanguage(language: lang_code)
        }
        alertController.addAction(action)
        
        self.present(alertController, animated: true)
    }
}
