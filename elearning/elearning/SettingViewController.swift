import UIKit
import UserNotifications

class SettingViewController : UIViewController {
    @IBOutlet weak var mTimePicker: UIDatePicker!
    @IBOutlet weak var mEnableText: UILabel!
    @IBOutlet weak var mEnableSwitch: UISwitch!
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    var mService : ELService!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mService = appDelegate.getELService()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        var oldTime = mService.getSavedTimeNoti()
        if (oldTime == "") {
            oldTime = "8:00"
        }
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat =  "HH:mm"
        let date = dateFormatter.date(from: oldTime)
        
        
        
        mTimePicker.setDate(date!, animated: true)
    }
    
    @IBAction func onEnableChange(_ sender: Any) {
        if (mEnableSwitch.isOn) {
            mEnableText.text = "Enable"
            mTimePicker.isEnabled = true
            mService.saveTimeNotiEnable(enable: true)
            doApply(isDismiss: false)
        }
        else {
            mEnableText.text = "Disable"
            mTimePicker.isEnabled = false
            mService.saveTimeNotiEnable(enable: false)
            
            let center = UNUserNotificationCenter.current()
            center.removeAllDeliveredNotifications()
            center.removeAllPendingNotificationRequests()
        }
    }
    
    
    @IBAction func onBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func onApply(_ sender: Any) {
        doApply(isDismiss: true)
    }
    
    func doApply(isDismiss : Bool) {
        let selectedDate = mTimePicker.date
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat =  "HH:mm"
        let time = dateFormatter.string(from: selectedDate)
        
        let timeSplit = time.characters.split(separator: ":").map(String.init)
        let hour = timeSplit[0]
        let minutes = timeSplit[1]
        
        let center = UNUserNotificationCenter.current()
        center.removeAllDeliveredNotifications()
        center.removeAllPendingNotificationRequests()
        
        var dateComponents = DateComponents()
        dateComponents.hour = Int(hour)
        dateComponents.minute = Int(minutes)
        let trigger = UNCalendarNotificationTrigger(dateMatching: dateComponents, repeats: true)
        
        let content = UNMutableNotificationContent()
        content.title = "English Learning"
        content.body = "Let's learn english by conversation"
        content.sound = UNNotificationSound.default()
        
        content.badge = UIApplication.shared.applicationIconBadgeNumber + 1 as NSNumber?
        let request = UNNotificationRequest(identifier: "every", content: content, trigger: trigger)
        
        center.add(request, withCompletionHandler: nil)
        
        mService.saveTimeNoti(time: time)
        
        if (isDismiss == true) {
            self.dismiss(animated: true, completion: nil)
        }
    }
}
