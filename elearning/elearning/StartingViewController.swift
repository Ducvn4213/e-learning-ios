import UIKit
import SideMenu

class StartingViewController : UIViewController {
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    var mService : ELService!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        mService = appDelegate.getELService()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        checkSavedLanguage()
    }
    
    func checkSavedLanguage() {
        let savedLanguage = mService.getSavedLanguage()
        if (savedLanguage == "") {
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let mainViewController = storyBoard.instantiateViewController(withIdentifier: "language_select")
            self.present(mainViewController, animated: false, completion: nil)
        }
        else {
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let mainViewController = storyBoard.instantiateViewController(withIdentifier: "main")
            self.present(mainViewController, animated: false, completion: nil)
        }
    }
}
