import JASON

extension JSONKeys {
    static let id    = JSONKey<String>("id")
    static let value    = JSONKey<String>("value")
    static let language    = JSONKey<String>("language")
}

struct LanguageResponseItem {
    var id : String
    var value : String
    var language : String
    
    init(_ json: JSON) {
        id    = json[.id]
        value = json[.value]
        language = json[.language]
    }
}
