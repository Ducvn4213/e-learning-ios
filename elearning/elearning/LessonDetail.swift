import JASON

extension JSONKeys {
    static let iid    = JSONKey<String>("id")
    static let lesson    = JSONKey<String>("lesson_id")
    static let title    = JSONKey<String>("title")
    static let meaning    = JSONKey<String>("meaning")
    static let audio    = JSONKey<String>("audio")
    static let audio_2    = JSONKey<String>("audio_2")
}

struct LessonDetail {
    var id: Int
    var lesson : Int
    var title : String
    var meaning : String
    var audio : String
    var audio2 : String
    var score : Int
    var timesave : Int
    
    init(_ json: JSON) {
        id    = Int(json[.iid])!
        lesson = Int(json[.lesson])!
        title = json[.title]
        meaning = json[.meaning]
        audio = json[.audio]
        audio2 = json[.audio_2]
        
        score = 0
        timesave = 0
    }
    
    init(_id : Int, _lesson : Int, _title : String, _meaning : String, _audio : String, _audio2 : String, _score : Int, _timesave : Int) {
        id = _id
        lesson = _lesson
        title = _title
        meaning = _meaning
        audio = _audio
        audio2 = _audio2
        score = _score
        timesave = _timesave
    }
}
