import UIKit
import Cosmos

class LessionItemCell: UITableViewCell {
    var mData : LessionItem!
    var mService : ELService!
    var itemSTTTabDelegate : ItemSTTTab!
    
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var title2: UILabel!
    @IBOutlet weak var meaning: UILabel!
    @IBOutlet weak var audio: UIButton!
    @IBOutlet weak var audio2: UIButton!
    @IBOutlet weak var speak: UIButton!
    @IBOutlet weak var done: CosmosView!
    @IBOutlet weak var done2: CosmosView!
    @IBOutlet weak var subContainer: UIView!
    @IBOutlet weak var container: UIView!
    @IBOutlet weak var container2: UIView!
    @IBOutlet weak var instructor: UIImageView!
    
    @IBAction func audioClick(_ sender: Any) {
        mService.playURL(url: mData.audio)
    }
    @IBAction func audio2Click(_ sender: Any) {
        var urlToPlay = mData.audio2
        if (mData.audio2 == "") {
            urlToPlay = mData.audio.replacingOccurrences(of: "/mp3/", with: "/slow/")
        }
        mService.playURL(url: urlToPlay)
    }
    
    func fixSize() {
        if (mService.originalTitleWidth == nil) {
            mService.originalTitleWidth = title.frame.size.width
        }
        if (mService.originalTitle2Width == nil) {
            mService.originalTitle2Width = title2.frame.size.width
        }
        if (mService.originalContainerHeight == nil) {
            mService.originalContainerHeight = subContainer.frame.size.height
        }
        
        title.frame.size.width = mService.originalTitleWidth
        title2.frame.size.width = mService.originalTitle2Width
        meaning.frame.size.width = mService.originalTitleWidth
        
        let oldH1 = title.frame.size.height
        let oldH2 = meaning.frame.size.height
        let oldH3 = title2.frame.size.height
        
        title.sizeToFit()
        meaning.sizeToFit()
        title2.sizeToFit()
        
        let newH1 = title.frame.size.height
        let newH2 = meaning.frame.size.height
        let newH3 = title2.frame.size.height
        
        let margin1 = oldH1 - newH1
        let margin2 = oldH2 - newH2
        let margin3 = oldH3 - newH3
        
        meaning.frame.origin.y -= margin1
        
        
        subContainer.frame.size.height -= (margin1 + margin2)
        container.frame.size.height = subContainer.frame.size.height
        container2.frame.origin.y -= (margin1 + margin2)
        instructor.frame.origin.y -= (margin1 + margin2)
        speak.frame.origin.y -= (margin1 + margin2)
        container2.frame.size.height -= margin3
        speak.frame.origin.y -= margin3
        
        title2.frame.size.width = mService.originalTitle2Width
    }
    
    func fixSize2() {
        if (mService.originalTitleWidth2 == nil) {
            mService.originalTitleWidth2 = title.frame.size.width
        }
        if (mService.originalTitle2Width2 == nil) {
            mService.originalTitle2Width2 = title2.frame.size.width
        }
        if (mService.originalContainerHeight2 == nil) {
            mService.originalContainerHeight2 = subContainer.frame.size.height
        }
        
        title.frame.size.width = mService.originalTitleWidth2
        title2.frame.size.width = mService.originalTitle2Width2
        meaning.frame.size.width = mService.originalTitleWidth2
        
        let oldH1 = title.frame.size.height
        let oldH2 = meaning.frame.size.height
        let oldH3 = title2.frame.size.height
        
        title.sizeToFit()
        meaning.sizeToFit()
        title2.sizeToFit()
        
        let newH1 = title.frame.size.height
        let newH2 = meaning.frame.size.height
        let newH3 = title2.frame.size.height
        
        let margin1 = oldH1 - newH1
        let margin2 = oldH2 - newH2
        let margin3 = oldH3 - newH3
        
        meaning.frame.origin.y -= margin1
        
        
        subContainer.frame.size.height -= (margin1 + margin2)
        container.frame.size.height = subContainer.frame.size.height
        container2.frame.origin.y -= (margin1 + margin2)
        instructor.frame.origin.y -= (margin1 + margin2)
        speak.frame.origin.y -= (margin1 + margin2)
        container2.frame.size.height -= margin3
        speak.frame.origin.y -= margin3
        
        title2.frame.size.width = mService.originalTitle2Width2
//        let titleSize = title.frame.size.width
//        let meaningSize = meaning.frame.size.width
//        let doneSize = done2.frame.size.width
//        
//        var width = titleSize > meaningSize ? titleSize : meaningSize
//        width = width > doneSize ? width : doneSize
//        width += 10
//        
//        let bigWidth = UIScreen.main.bounds.width
//        
//        container.frame.size.width = width + 35
//        container.frame.origin.x = bigWidth - container.frame.size.width - 5
        
    }
    
    @IBAction func onSpeak(_ sender: Any) {
        itemSTTTabDelegate.onSTTTab(id: self.mData.id, sentence: self.mData.sentence)
    }
}
